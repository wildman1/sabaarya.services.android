package com.sabaarya.services.data.network.interceptors;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.sabaarya.services.data.network.models.Result;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ResponseParserInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        Result result = new Gson().fromJson(response.body().string(), Result.class);
        result.headers = response.headers().toMultimap();
        String responseString = new Gson().toJson(result);
        return new Response.Builder()
                .code(response.code())
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_0)
                .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
                .addHeader("content-type", "application/json")
                .build();
    }

}