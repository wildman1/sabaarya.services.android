package com.sabaarya.services.data.network;

import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.network.models.AuthCheck;
import com.sabaarya.services.data.network.models.HomeData;
import com.sabaarya.services.data.network.models.ProfileData;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.network.models.User;
import com.sabaarya.services.data.network.requests.CheckEmailRequest;
import com.sabaarya.services.data.network.requests.CheckPhoneRequest;
import com.sabaarya.services.data.network.requests.ConfirmationCodeEmailRequest;
import com.sabaarya.services.data.network.requests.ConfirmationCodePhoneRequest;
import com.sabaarya.services.data.network.requests.RegisterEmailRequest;
import com.sabaarya.services.data.network.requests.RegisterPhoneRequest;

import io.reactivex.Flowable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ServerAPI {
    String HOST = "files.sazin.com";
    String SERVER_DOMEN = "http://" + HOST + "/api/mobileapp/";

    @GET("http://s.sarzin.com/api/mobileapp/GetVersion?geoid=a&os=0")
    Flowable<Result<AppVersion>>
    getVersion(@Query("languageid") int languageId);

    @GET("Gethomedata")
    Flowable<Result<HomeData>>
    getHomeData(@Query("userId") int userId, @Query("languageid") int languageId);

    @POST("PostConfirmationCode")
    Flowable<Result<Boolean>>
    confirmEmail(@Body ConfirmationCodeEmailRequest request);

    @POST("CheckAccount")
    Flowable<Result<AuthCheck>>
    checkAccount(@Body CheckPhoneRequest request);

    @POST("CheckAccount")
    Flowable<Result<AuthCheck>>
    checkAccount(@Body CheckEmailRequest request);

    @POST("PostConfirmationCode")
    Flowable<Result<Boolean>>
    confirmPhone(@Body ConfirmationCodePhoneRequest request);

    @POST("PostUser")
    Flowable<Result<User>>
    register(@Body RegisterEmailRequest request);

    @POST("PostUser")
    Flowable<Result<User>>
    register(@Body RegisterPhoneRequest request);

    @GET("GetBrandInfo")
    Flowable<Result<ProfileData>>
    getProfileData(@Query("CompanyId") int companyId, @Query("userId") int userId, @Query("languageid") int languageId);


}
