package com.sabaarya.services.data.database.base;

public abstract class Mapping<From, To> {

    public abstract To map(From from);
}
