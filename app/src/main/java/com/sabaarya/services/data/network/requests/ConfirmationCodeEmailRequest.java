package com.sabaarya.services.data.network.requests;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class ConfirmationCodeEmailRequest extends LanguageIdRequest {

    @SerializedName("email")
    public String email;


    public ConfirmationCodeEmailRequest(@NonNull String email, int languageId) {
        super(languageId);
        this.email = email;
    }

}
