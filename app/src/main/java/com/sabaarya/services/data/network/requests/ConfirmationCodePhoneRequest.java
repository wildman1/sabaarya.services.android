package com.sabaarya.services.data.network.requests;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.sabaarya.services.utils.Values;

public class ConfirmationCodePhoneRequest extends LanguageIdRequest {

    @SerializedName("username")
    public String phone;
    @SerializedName("code")
    public String code = Values.COUNTRY_CODE;


    public ConfirmationCodePhoneRequest(@NonNull String phone, int languageId) {
        super(languageId);
        this.phone = phone;
    }

}
