package com.sabaarya.services.data.network.requests;

import com.google.gson.annotations.SerializedName;

class LanguageIdRequest {

    @SerializedName("languageid")
    public int languageId;

    LanguageIdRequest(int languageId) {
        this.languageId = languageId;
    }

}
