package com.sabaarya.services.data.network.requests;

import com.google.gson.annotations.SerializedName;

public class RegisterEmailRequest extends RegisterRequest {

    @SerializedName("email")
    public String email;

    public RegisterEmailRequest(String name, String confirmationCode, String email, int languageId) {
        super(name, confirmationCode, languageId);
        this.email = email;
    }
}
