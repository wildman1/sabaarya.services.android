package com.sabaarya.services.data.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppVersion {

    @SerializedName("AppVersion")
    public int appVersion;
    @SerializedName("AppLink")
    public String appLink;
    @SerializedName("GeoVersion")
    public int geoVersion;
    @SerializedName("GeoLink")
    public String geoLink;
    @SerializedName("CatVersion")
    public int categoriesVersion;
    @SerializedName("CatLink")
    public String categoriesLink;
    @SerializedName("ImageServer")
    public String imageServer;
    @SerializedName("CatId")
    public String catId;
    @SerializedName("UpdateTitle")
    public String updateTitle;
    @SerializedName("Slide")
    public List<HomeSlide> slides;

}
