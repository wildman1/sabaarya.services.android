package com.sabaarya.services.data.network.interceptors;

import com.sabaarya.services.data.preference.Preferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class ServiceParamsInterceptor implements Interceptor {
    private Preferences preferences;

    public ServiceParamsInterceptor(Preferences preferences) {
        this.preferences = preferences;

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
            Request.Builder builder = chain.request().newBuilder()
                    .header("Accept-Encoding", "identity")
                    .header("Accept", "application/json");
            Request compressedRequest = builder.build();
            return chain.proceed(compressedRequest);
    }

}
