package com.sabaarya.services.data.database.mappers;


import com.sabaarya.services.data.database.base.Mapping;
import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.network.models.Category;

import java.util.ArrayList;
import java.util.List;

public class DbToApiCategoryListMapper extends Mapping<List<DBCategory>, List<Category>> {

    private final DbToApiCategoryMapper mapper = new DbToApiCategoryMapper();

    @Override
    public List<Category> map(List<DBCategory> dbCategories) {
        ArrayList<Category> categories = new ArrayList<>(dbCategories.size());
        for (DBCategory dbCategory : dbCategories) {
            categories.add(mapper.map(dbCategory));
        }
        return categories;
    }
}
