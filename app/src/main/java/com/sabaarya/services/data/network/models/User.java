package com.sabaarya.services.data.network.models;


import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("Address")
    public String address;
    @SerializedName("Phone")
    public String phone;
    @SerializedName("CompanyId")
    public int companyId;
    @SerializedName("isFirstLogin")
    public boolean firstLogin;
    @SerializedName("UID")
    public String uid;
    @SerializedName("CompanyName")
    public String companyName;
    @SerializedName("MapAddress")
    public String mapAddress;
    @SerializedName("LanguageId")
    public int languageId;
    @SerializedName("userInformation")
    public UserInformation information;
    @SerializedName("TemporaryToken")
    public String accessToken;
}
