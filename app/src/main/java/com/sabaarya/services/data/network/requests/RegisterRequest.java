package com.sabaarya.services.data.network.requests;


import com.google.gson.annotations.SerializedName;

class RegisterRequest extends LanguageIdRequest {

    @SerializedName("v")
    public String version = "1";
    @SerializedName("name")
    public String name;
    @SerializedName("uid")
    public String uid = "";
    @SerializedName("password")
    public String password = "";
    @SerializedName("ConfirmationCode")
    public String confirmationCode;

    RegisterRequest(String name, String confirmationCode, int languageId) {
        super(languageId);
        this.name = name;
        this.confirmationCode = confirmationCode;
    }
}
