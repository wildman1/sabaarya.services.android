package com.sabaarya.services.data.database.mappers;


import com.sabaarya.services.data.database.base.Mapping;
import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.network.models.Category;

import java.util.ArrayList;
import java.util.List;

public class ApiToDbCategoryListMapper extends Mapping<List<Category>, List<DBCategory>> {

    private final ApiToDbCategoryMapper mapper = new ApiToDbCategoryMapper();

    @Override
    public List<DBCategory> map(List<Category> categories) {
        ArrayList<DBCategory> dbCategories = new ArrayList<>(categories.size());
        for (Category category : categories) {
            dbCategories.add(mapper.map(category));
        }
        return dbCategories;
    }
}
