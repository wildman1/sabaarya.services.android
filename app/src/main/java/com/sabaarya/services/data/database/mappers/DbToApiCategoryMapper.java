package com.sabaarya.services.data.database.mappers;


import com.sabaarya.services.data.database.base.Mapping;
import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.network.models.Category;

public class DbToApiCategoryMapper extends Mapping<DBCategory, Category> {

    @Override
    public Category map(DBCategory dbCategory) {
        Category category = new Category();
        category.setId(dbCategory.getId());
        category.setTitle(dbCategory.getTitle());
        category.setCount(dbCategory.getCount());
        category.setImage(dbCategory.getImage());
        category.hasChildren = dbCategory.isHasChildren();
        return category;
    }
}
