package com.sabaarya.services.data.database.mappers;


import com.sabaarya.services.data.database.base.Mapping;
import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.network.models.Category;

public class ApiToDbCategoryMapper extends Mapping<Category, DBCategory> {

    @Override
    public DBCategory map(Category category) {
        DBCategory dbCategory = new DBCategory();
        dbCategory.setId(category.id);
        dbCategory.setTitle(category.title);
        dbCategory.setCount(category.count);
        dbCategory.setImage(category.image);
        dbCategory.setParent(category.id.substring(0, category.id.length() - 1));
        return dbCategory;
    }
}
