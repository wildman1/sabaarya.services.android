package com.sabaarya.services.data.network.models;

import com.google.gson.annotations.SerializedName;

public class AuthCheck extends User {

    public static final int RESULT_WRONG = 0;
    public static final int RESULT_NEW = 1;
    public static final int RESULT_LOGIN = 2;
    public static final int RESULT_PASSWORD = 3;

    @SerializedName("R")
    public int result;

}
