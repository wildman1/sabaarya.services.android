package com.sabaarya.services.data.network.models;

import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("MyGems")
    public double myGems;
    @SerializedName("MyPrices")
    public double myPrices;

}
