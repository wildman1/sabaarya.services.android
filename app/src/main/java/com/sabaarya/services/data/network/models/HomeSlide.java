package com.sabaarya.services.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class HomeSlide implements Parcelable {

    public static final int TYPE_CATEGORY = 1;
    public static final int TYPE_LINK = 7;


    @SerializedName("catId")
    public String catId;
    @SerializedName("Link")
    public String link;
    @SerializedName("ThumbnailUrl")
    public String thumbnail;
    @SerializedName("Title")
    public String title;
    @SerializedName("Image")
    public String image;
    @SerializedName("TypeId")
    public int typeId;


    protected HomeSlide(Parcel in) {
        catId = in.readString();
        link = in.readString();
        thumbnail = in.readString();
        title = in.readString();
        image = in.readString();
        typeId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(catId);
        dest.writeString(link);
        dest.writeString(thumbnail);
        dest.writeString(title);
        dest.writeString(image);
        dest.writeInt(typeId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<HomeSlide> CREATOR = new Parcelable.Creator<HomeSlide>() {
        @Override
        public HomeSlide createFromParcel(Parcel in) {
            return new HomeSlide(in);
        }

        @Override
        public HomeSlide[] newArray(int size) {
            return new HomeSlide[size];
        }
    };
}