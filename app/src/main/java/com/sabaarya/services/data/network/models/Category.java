package com.sabaarya.services.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Category implements Parcelable {

    @SerializedName("Id")
    public String id;
    @SerializedName("T")
    public String title;
    @SerializedName("c")
    public int count;
    @SerializedName("I")
    public String image;
    @SerializedName("has_parent")
    public boolean hasChildren;

    public Category(String id) {
        this.id = id;
    }

    public Category() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    protected Category(Parcel in) {
        id = in.readString();
        title = in.readString();
        count = in.readInt();
        image = in.readString();
        hasChildren = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeInt(count);
        dest.writeString(image);
        dest.writeByte((byte) (hasChildren ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
