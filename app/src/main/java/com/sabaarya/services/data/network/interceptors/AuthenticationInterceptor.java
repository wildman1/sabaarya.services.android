package com.sabaarya.services.data.network.interceptors;

import android.support.annotation.NonNull;

import com.sabaarya.services.data.preference.Preferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class AuthenticationInterceptor implements Interceptor {
    private Preferences preferences;

    public AuthenticationInterceptor(Preferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder()
                .header("Accept-Encoding", "identity");
        String accessToken = preferences.getAccessToken();
        if (accessToken != null) {
            builder.header("Token", accessToken);
        }
        Request compressedRequest = builder.build();
        return chain.proceed(compressedRequest);
    }

}
