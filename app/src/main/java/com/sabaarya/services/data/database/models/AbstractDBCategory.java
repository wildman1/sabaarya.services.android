package com.sabaarya.services.data.database.models;

import io.requery.Entity;
import io.requery.Key;

@Entity
public class AbstractDBCategory {

    @Key
    String id;
    String title;
    int count;
    String image;
    String parent;
    boolean hasChildren;

}
