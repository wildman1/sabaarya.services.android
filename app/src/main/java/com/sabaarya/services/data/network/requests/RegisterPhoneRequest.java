package com.sabaarya.services.data.network.requests;

import com.google.gson.annotations.SerializedName;
import com.sabaarya.services.utils.Values;

public class RegisterPhoneRequest extends RegisterRequest {

    @SerializedName("username")
    public String phone;
    @SerializedName("code")
    public String code = Values.COUNTRY_CODE;

    public RegisterPhoneRequest(String name, String confirmationCode, String phone, int languageId) {
        super(name, confirmationCode, languageId);
        this.phone = phone;
    }

}
