package com.sabaarya.services.data.network.models;

import com.google.gson.annotations.SerializedName;

public class UserInformation {

    @SerializedName("Logo")
    public String logo;
    @SerializedName("MyGems")
    public double myGems;
    @SerializedName("MyPrices")
    public double myPrices;
    @SerializedName("NotificatinCount")
    public int notificationsCount;
    @SerializedName("MoneyId")
    public int moneyId;
    @SerializedName("GeoId")
    public String geoId;
    @SerializedName("Latitude")
    public double latitude;
    @SerializedName("Longitude")
    public double longitude;

}
