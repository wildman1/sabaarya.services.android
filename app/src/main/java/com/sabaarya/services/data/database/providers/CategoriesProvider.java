package com.sabaarya.services.data.database.providers;

import android.support.annotation.NonNull;

import com.sabaarya.services.data.database.base.AbstractProvider;
import com.sabaarya.services.data.database.mappers.ApiToDbCategoryListMapper;
import com.sabaarya.services.data.database.mappers.DbToApiCategoryListMapper;
import com.sabaarya.services.data.database.mappers.DbToApiCategoryMapper;
import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.network.models.Category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

@Singleton
public class CategoriesProvider extends AbstractProvider {

    @Inject
    public CategoriesProvider(ReactiveEntityStore<Persistable> entityStore) {
        super(entityStore);
    }

    public Flowable<Iterable<DBCategory>> putCategories(@NonNull List<Category> categories) {
        entityStore.delete(DBCategory.class).get().value();
        List<DBCategory> dbCategories = new ApiToDbCategoryListMapper().map(categories);
        Map<String, DBCategory> categoriesMap = new HashMap<>(categories.size());
        for (DBCategory dbCategory : dbCategories) {
            categoriesMap.put(dbCategory.getId(), dbCategory);
        }
        for (DBCategory dbCategory : dbCategories) {
            DBCategory category = categoriesMap.get(dbCategory.getParent());
            if (category != null) {
                category.setHasChildren(true);
            }
        }
        return entityStore.upsert(dbCategories)
                .subscribeOn(Schedulers.io())
                .toFlowable();
    }

    public Flowable<List<Category>> getCategoriesByParent(String query) {
        return entityStore
                .select(DBCategory.class)
                .where(DBCategory.PARENT.lower().eq(query.toLowerCase()))
                .get()
                .observable()
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbCategories -> new DbToApiCategoryListMapper().map(dbCategories));
    }

    public Flowable<List<Category>> getCategoriesByParentAndChildrenPresent(String query) {
        return entityStore
                .select(DBCategory.class)
                .where(DBCategory.PARENT.lower().eq(query.toLowerCase()).and(DBCategory.HAS_CHILDREN.eq(Boolean.TRUE)))
                .get()
                .observable()
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbCategories -> new DbToApiCategoryListMapper().map(dbCategories));
    }

    public Flowable<Category> getCategoryById(String query) {
        return entityStore
                .select(DBCategory.class)
                .where(DBCategory.ID.lower().eq(query.toLowerCase()))
                .limit(1)
                .get()
                .observable()
                .first(new DBCategory())
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbCategory -> new DbToApiCategoryMapper().map(dbCategory));
    }

    public Flowable<List<Category>> searchCategoriesByTitle(String query) {
        return entityStore
                .select(DBCategory.class)
                .where(DBCategory.TITLE.lower().like("%" + query.toLowerCase() + "%"))
                .get()
                .observable()
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbCategories -> new DbToApiCategoryListMapper().map(dbCategories));
    }


}
