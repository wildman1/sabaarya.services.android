package com.sabaarya.services.data.preference;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.network.models.User;


public class Preferences {

    private static final Gson GSON = new Gson();

    private static final String KEY_FIRST_RUN = "FIRST_RUN";
    private static final String KEY_USER = "USER";
    private static final String KEY_USER_BALANCE = "USER_BALANCE";
    private static final String KEY_LANGUAGE = "LANGUAGE";

    private static final String KEY_APP_VERSION = "app_version";
    private static final String KEY_CATEGORIES_VERSION = "categories_version";


    private final SharedPreferences sharedPreferences;


    public Preferences(SharedPreferences preferences) {
        sharedPreferences = preferences;
    }

    public void setUser(User user) {
        sharedPreferences.edit().putString(KEY_USER, GSON.toJson(user)).apply();
    }

    public User getUser() {
        return GSON.fromJson(sharedPreferences.getString(KEY_USER, null), User.class);
    }

    public String getAccessToken() {
        User user = getUser();
        if (user != null) {
            return user.accessToken;
        }
        return null;
    }

    public void setAppVersion(AppVersion appVersion) {
        sharedPreferences.edit().putString(KEY_APP_VERSION, GSON.toJson(appVersion)).apply();
    }

    public AppVersion getAppVersion() {
        return GSON.fromJson(sharedPreferences.getString(KEY_APP_VERSION, null), AppVersion.class);
    }

    public void setLanguage(int language) {
        sharedPreferences.edit().putInt(KEY_LANGUAGE, language).apply();
    }

    public int getLanguage() {
        return sharedPreferences.getInt(KEY_LANGUAGE, 0);
    }

    public int getLanguageAPI() {
        return getLanguage() + 1;
    }

    public void setCategoriesVersion(int categoriesVersion) {
        sharedPreferences.edit().putInt(KEY_CATEGORIES_VERSION, categoriesVersion).apply();
    }

    public int getCategoriesVersion() {
        return sharedPreferences.getInt(KEY_CATEGORIES_VERSION, 0);
    }

    public void setUserBalance(float balance) {
        sharedPreferences.edit().putFloat(KEY_USER_BALANCE, balance).apply();
    }

    public double getUserBalance() {
        return sharedPreferences.getFloat(KEY_USER_BALANCE, 0);
    }

    public boolean isFirstRun() {
        return sharedPreferences.getBoolean(KEY_FIRST_RUN, true);
    }

    public void setFirstRun() {
        sharedPreferences.edit().putBoolean(KEY_FIRST_RUN, false).apply();
    }


}
