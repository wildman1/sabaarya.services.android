package com.sabaarya.services.data.network;

import com.sabaarya.services.data.network.models.Category;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Url;


public interface RawAPI {

    @GET
    Flowable<List<Category>>
    getCategories(@Url String url);


}
