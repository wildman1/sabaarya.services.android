package com.sabaarya.services.data.network.requests;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.sabaarya.services.utils.Values;

public class CheckPhoneRequest extends CheckAccountRequest {

    @SerializedName("username")
    public String phone;
    @SerializedName("code")
    public String code = Values.COUNTRY_CODE;


    public CheckPhoneRequest(@NonNull String confirmationCode, @NonNull String phone, int languageId) {
        super(confirmationCode, languageId);
        this.phone = phone;
    }

}
