package com.sabaarya.services.data.network.requests;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

class CheckAccountRequest extends LanguageIdRequest {

    @SerializedName("ConfirmationCode")
    public String confirmationCode;


    CheckAccountRequest(@NonNull String confirmationCode, int languageId) {
        super(languageId);
        this.confirmationCode = confirmationCode;
    }

}