package com.sabaarya.services.data.database.base;

import android.support.annotation.NonNull;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

public abstract class AbstractProvider {

    protected final ReactiveEntityStore<Persistable> entityStore;

    public AbstractProvider(@NonNull ReactiveEntityStore<Persistable> entityStore) {
        this.entityStore = entityStore;
    }
}
