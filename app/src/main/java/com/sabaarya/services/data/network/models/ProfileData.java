package com.sabaarya.services.data.network.models;

import com.google.gson.annotations.SerializedName;

public class ProfileData {

    @SerializedName("Logo")
    public String imageUrl;
    @SerializedName("CompanyName")
    public String companyName;

}
