package com.sabaarya.services.data.network.requests;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class CheckEmailRequest extends CheckAccountRequest {

    @SerializedName("email")
    public String email;


    public CheckEmailRequest(@NonNull String confirmationCode, @NonNull String email, int languageId) {
        super(confirmationCode, languageId);
        this.email = email;
    }

}
