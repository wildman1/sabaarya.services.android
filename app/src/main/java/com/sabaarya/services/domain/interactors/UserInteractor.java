package com.sabaarya.services.domain.interactors;


import com.sabaarya.services.data.network.models.ProfileData;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.network.models.User;
import com.sabaarya.services.data.preference.Preferences;
import com.sabaarya.services.repositories.UserRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class UserInteractor {

    private final Preferences preferences;
    private final UserRepository userRepository;

    @Inject
    public UserInteractor(Preferences preferences, UserRepository userRepository) {
        this.preferences = preferences;
        this.userRepository = userRepository;
    }

    public boolean isUserLogged() {
        return preferences.getUser() != null;
    }

    public void saveUser(User user) {
        preferences.setUser(user);
    }

    public int getUserId() {
        User user = getUserFromPrefs();
        if (user != null) {
            return user.companyId;
        }
        return 0;
    }

    public User getUserFromPrefs() {
        return preferences.getUser();
    }

    public void setUserBalance(float balance) {
        preferences.setUserBalance(balance);
    }

    public double getUserBalance() {
        return preferences.getUserBalance();
    }

    public boolean isFirstRun() {
        return preferences.isFirstRun();
    }

    public void setFirstRun() {
        preferences.setFirstRun();
    }

    public Flowable<Result<ProfileData>> getProfileData(int companyId, int requesterId) {
        return userRepository.getProfileInfo(companyId, requesterId);
    }

}
