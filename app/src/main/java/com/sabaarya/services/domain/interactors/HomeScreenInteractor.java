package com.sabaarya.services.domain.interactors;

import com.sabaarya.services.data.database.models.DBCategory;
import com.sabaarya.services.data.database.providers.CategoriesProvider;
import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.data.network.models.HomeData;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.repositories.HomeRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class HomeScreenInteractor {

    private final HomeRepository homeRepository;
    private final CategoriesProvider categoriesProvider;

    @Inject
    HomeScreenInteractor(HomeRepository homeRepository, CategoriesProvider categoriesProvider) {
        this.homeRepository = homeRepository;
        this.categoriesProvider = categoriesProvider;
    }

    public Flowable<Result<AppVersion>> getAppVersion() {
        return homeRepository.getAppVersion();
    }

    public Flowable<Result<HomeData>> getHomeData(int userId) {
        return homeRepository.getHomeData(userId);
    }

    public Flowable<List<Category>> getCategories(String url) {
        return homeRepository.getCategories(url);
    }

    public Flowable<Iterable<DBCategory>> saveCategories(List<Category> categories) {
        return categoriesProvider.putCategories(categories);
    }

    public Flowable<List<Category>> getCategoriesByParent(String parent) {
        return categoriesProvider.getCategoriesByParent(parent);
    }

}
