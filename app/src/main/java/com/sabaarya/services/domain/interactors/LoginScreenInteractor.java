package com.sabaarya.services.domain.interactors;

import com.sabaarya.services.data.network.models.AuthCheck;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.network.models.User;
import com.sabaarya.services.repositories.LoginRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class LoginScreenInteractor {

    private final LoginRepository loginRepository;

    @Inject
    public LoginScreenInteractor(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public Flowable<Result<Boolean>> confirmEmail(String email) {
        return loginRepository.confirmEmail(email);
    }

    public Flowable<Result<Boolean>> confirmPhone(String phone) {
        return loginRepository.confirmPhone(phone);
    }

    public Flowable<Result<AuthCheck>> checkPhone(String confirmationCode, String phone) {
        return loginRepository.checkPhone(confirmationCode, phone);
    }

    public Flowable<Result<AuthCheck>> checkEmail(String confirmationCode, String email) {
        return loginRepository.checkEmail(confirmationCode, email);
    }

    public Flowable<Result<User>> registerWithPhone(String name, String confirmationCode, String phone) {
        return loginRepository.registerWithPhone(name, confirmationCode, phone);
    }

    public Flowable<Result<User>> registerWithEmail(String name, String confirmationCode, String email) {
        return loginRepository.registerWithEmail(name, confirmationCode, email);
    }

}
