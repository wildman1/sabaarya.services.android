package com.sabaarya.services.domain.interactors;

import com.sabaarya.services.data.database.providers.CategoriesProvider;
import com.sabaarya.services.data.network.models.Category;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class CategoriesListInteractor {

    private final CategoriesProvider categoriesProvider;

    @Inject
    public CategoriesListInteractor(CategoriesProvider categoriesProvider) {
        this.categoriesProvider = categoriesProvider;
    }

    public Flowable<List<Category>> getCategoriesWithChildrenByParent(String parent) {
        return categoriesProvider.getCategoriesByParentAndChildrenPresent(parent);
    }

    public Flowable<List<Category>> getCategoriesByParent(String parent) {
        return categoriesProvider.getCategoriesByParent(parent);
    }

    public Flowable<Category> getCategoryById(String id) {
        return categoriesProvider.getCategoryById(id);
    }

    public Flowable<List<Category>> searchCategoriesByTitle(String query) {
        return categoriesProvider.searchCategoriesByTitle(query);
    }

}
