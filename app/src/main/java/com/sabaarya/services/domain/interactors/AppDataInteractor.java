package com.sabaarya.services.domain.interactors;


import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.preference.Preferences;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppDataInteractor {

    private final Preferences preferences;

    @Inject
    public AppDataInteractor(Preferences preferences) {
        this.preferences = preferences;
    }

    public void saveAppVersion(AppVersion appVersion) {
        preferences.setAppVersion(appVersion);
    }

    public AppVersion getAppVersionFromPrefs() {
        return preferences.getAppVersion();
    }

    public void setLanguage(int language) {
        preferences.setLanguage(language);
    }

    public int getLanguage() {
        return preferences.getLanguage();
    }


    public void setCategoriesVersion(int categoriesVersion) {
        preferences.setCategoriesVersion(categoriesVersion);
    }

    public int getCategoriesVersion() {
        return preferences.getCategoriesVersion();
    }


}
