package com.sabaarya.services.presentation.login.send_confirmation;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ViewSwitcher;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.view.AfterTextWatcher;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.OnClick;

public class SendConfirmationCodeFragment extends MvpBaseFragment implements BackButtonListener, SendConfirmationCodeView {

    @BindView(R.id.email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone_layout)
    TextInputLayout phoneLayout;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.switcher)
    ViewSwitcher switcher;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    SendConfirmationCodePresenter presenter;

    @ProvidePresenter
    SendConfirmationCodePresenter provideConfirmationPresenter() {
        return new SendConfirmationCodePresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_send_confirmation_code;
    }


    public static Fragment getNewInstance() {
        return new SendConfirmationCodeFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        phone.addTextChangedListener(new AfterTextWatcher(s -> phoneLayout.setError(null)));
        email.addTextChangedListener(new AfterTextWatcher(s -> emailLayout.setError(null)));
        tabs.addTab(tabs.newTab().setText(R.string.phone), true);
        tabs.addTab(tabs.newTab().setText(R.string.email));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switcher.showNext();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        new RxPermissions(getActivity()).request(Manifest.permission.READ_PHONE_STATE)
                .subscribe(granted -> {
                    if (granted) {
                        presetLogin();
                    }
                });
        toolbar.inflateMenu(R.menu.menu_settings_icon);
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.action_settings) {
                presenter.openSettings();
            }
            return true;
        });
    }

    @OnClick(R.id.send)
    void onClickLogin() {
        if (switcher.getDisplayedChild() == 0) {
            presenter.confirmPhone(phone.getText().toString(), phone.getText().toString());
        } else {
            presenter.confirmEmail(email.getText().toString());
        }
    }

    @SuppressLint("MissingPermission")
    private void presetLogin() {
        if (getContext() != null) {
            TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                String number = telephonyManager.getLine1Number();
                if (!TextUtils.isEmpty(number)) {
                    phone.setText(number.substring(2));
                }
            }
        }
    }

    @Override
    public void showPhoneError() {
        phoneLayout.setError(getString(R.string.mobile_error));
    }

    @Override
    public void showEmailError() {
        emailLayout.setError(getString(R.string.email_error));
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
