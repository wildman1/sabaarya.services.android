package com.sabaarya.services.presentation.container;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.presentation.base.MvpBaseActivity;
import com.sabaarya.services.presentation.base.RouterProvider;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.categories_list.CategoriesListFragment;
import com.sabaarya.services.presentation.category.CategoryFragment;
import com.sabaarya.services.presentation.category_children.CategoryStandaloneChildrenFragment;
import com.sabaarya.services.presentation.login.confirmation_code.ConfirmationCodeFragment;
import com.sabaarya.services.presentation.login.profile_register.ProfileRegisterFragment;
import com.sabaarya.services.presentation.search_services.SearchServicesFragment;
import com.sabaarya.services.presentation.settings.language.LanguageFragment;
import com.sabaarya.services.presentation.settings.main.SettingsFragment;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;


public class ContainerActivity extends MvpBaseActivity implements ContainerView, RouterProvider {

    private static final String SCREEN_NAME = "screen_name";
    private static final String SCREEN_DATA = "screen_data";

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    public ContainerPresenter presenter;

    @ProvidePresenter
    public ContainerPresenter createPresenter() {
        return new ContainerPresenter(router);
    }

    public static void start(Context context, String screenName, Bundle data) {
        context.startActivity(intent(context, screenName, data));
    }

    @NonNull
    private static Intent intent(Context context, String screenName, Bundle data) {
        return new Intent(context, ContainerActivity.class)
                .putExtra(SCREEN_NAME, screenName)
                .putExtra(SCREEN_DATA, data);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_container;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            presenter.startScreen(getIntent().getStringExtra(SCREEN_NAME), getIntent().getBundleExtra(SCREEN_DATA));
        }
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    private Navigator navigator = new SupportAppNavigator(this, R.id.container) {
        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            return null;
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (Screens.valueOf(screenKey)) {
                case CONFIRMATION_CODE_SCREEN:
                    return ConfirmationCodeFragment.getNewInstance((Bundle) data);
                case PROFILE_REGISTER_SCREEN:
                    return ProfileRegisterFragment.getNewInstance((Bundle) data);
                case CATEGORY_SCREEN:
                    return CategoryFragment.getNewInstance((Bundle) data);
                case CATEGORIES_LIST_SCREEN:
                    return CategoriesListFragment.getNewInstance((Bundle) data);
                case CATEGORY_STANDALONE_CHILDREN_SCREEN:
                    return CategoryStandaloneChildrenFragment.getNewInstance((Bundle) data);
                case SEARCH_SERVICES_SCREEN:
                    return SearchServicesFragment.getNewInstance();
                case SETTINGS_SCREEN:
                    return SettingsFragment.getNewInstance();
                case LANGUAGE_SCREEN:
                    return LanguageFragment.getNewInstance();
            }
            return null;
        }

        @Override
        protected void setupFragmentTransactionAnimation(
                Command command,
                Fragment currentFragment,
                Fragment nextFragment,
                FragmentTransaction fragmentTransaction) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
        }
    };

    @Override
    public Router getRouter() {
        return router;
    }

}
