package com.sabaarya.services.presentation.notifications;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class NotificationsPresenter extends MvpBasePresenter<NotificationsView> {
    private Router router;

    NotificationsPresenter(Router router) {
        this.router = router;
    }

}
