package com.sabaarya.services.presentation.base;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;


public interface SimpleErrorView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showConnectionError();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showServerError(Throwable throwable);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showErrorMessage(String s);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showInternalError();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showErrorMessage(int errorConnection);
}
