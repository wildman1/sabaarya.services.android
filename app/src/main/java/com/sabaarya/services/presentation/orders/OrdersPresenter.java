package com.sabaarya.services.presentation.orders;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class OrdersPresenter extends MvpBasePresenter<OrdersView> {
    private Router router;

    OrdersPresenter(Router router) {
        this.router = router;
    }

}
