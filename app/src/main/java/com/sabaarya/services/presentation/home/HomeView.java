package com.sabaarya.services.presentation.home;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.data.network.models.HomeSlide;
import com.sabaarya.services.presentation.base.MvpBaseView;

import java.util.List;


@StateStrategyType(AddToEndSingleStrategy.class)
interface HomeView extends MvpBaseView {

    void showHomeData(List<HomeSlide> slides);

    void showCategories(String imageServer, List<Category> categories);

    void setBalance(double balance);

    void showNewVersion();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showUpdateDialog(String title, String link);

}
