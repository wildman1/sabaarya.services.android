package com.sabaarya.services.presentation.profile;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class ProfilePresenter extends MvpBasePresenter<ProfileView> {

    @Inject
    UserInteractor userInteractor;

    private Router router;

    ProfilePresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        userInteractor
                .getProfileData(userInteractor.getUserId(), userInteractor.getUserId())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        result -> getViewState().showProfileInfo(result.data),
                        throwable -> {
                            getViewState().showProfileError();
                            getViewState().showServerError(throwable);
                        });
    }

    void openSettings() {
        router.navigateTo(Screens.SETTINGS_SCREEN.name());
    }

}
