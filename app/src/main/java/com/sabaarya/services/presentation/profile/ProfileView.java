package com.sabaarya.services.presentation.profile;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.data.network.models.ProfileData;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface ProfileView extends MvpBaseView {

    void showProfileInfo(ProfileData profileData);

    void showProfileError();

}
