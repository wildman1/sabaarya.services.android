package com.sabaarya.services.presentation.login.profile_register;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface ProfileRegisterView extends MvpBaseView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showNameError();

}
