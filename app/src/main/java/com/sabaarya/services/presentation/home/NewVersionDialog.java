package com.sabaarya.services.presentation.home;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.Window;
import android.widget.TextView;

import com.sabaarya.com.services.R;


public class NewVersionDialog extends Dialog {

    public NewVersionDialog(final Context context, final String text, final String link) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        setContentView(R.layout.dialog_new_version);
        ((TextView) findViewById(R.id.text)).setText(text);
        findViewById(R.id.yes).setOnClickListener(view -> {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            dismiss();
        });
        findViewById(R.id.cancel).setOnClickListener(view -> dismiss());
    }

}