package com.sabaarya.services.presentation.category_children;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

import java.util.List;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;

public class CategoryChildrenFragment extends MvpBaseFragment implements BackButtonListener, CategoryChildrenView {

    private static final String CATEGORY_KEY = "category_key";

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @InjectPresenter
    CategoryChildrenPresenter presenter;
    @ProvidePresenter
    CategoryChildrenPresenter provideConfirmationPresenter() {
        return new CategoryChildrenPresenter(getActivityRouter(), getArguments().getString(CATEGORY_KEY));
    }

    private ChildrenCategoriesAdapter adapter;
    private Disposable disposable;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_category_children;
    }


    public static Fragment getNewInstance(Bundle arguments) {
        CategoryChildrenFragment fragment = new CategoryChildrenFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static Bundle getBundle(String categoryId) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_KEY, categoryId);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new ChildrenCategoriesAdapter();
        if (disposable != null) disposable.dispose();
        disposable = adapter.clicks().subscribe(item -> presenter.onCategoryClicked(item));
        addSub(disposable);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showCategories(String imageServer, List<Category> categories) {
        adapter.setDataset(imageServer, categories);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
