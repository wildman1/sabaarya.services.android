package com.sabaarya.services.presentation.main;

import android.os.Handler;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainPresenter extends MvpBasePresenter<MainView> {
    private Router router;

    @Inject
    UserInteractor userInteractor;

    private boolean doubleBackToExitPressedOnce;

    MainPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            getViewState().exit();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        getViewState().showErrorMessage(R.string.press_back_again_to_exit);
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2_000);
    }

    void onTabMainClick() {
        router.replaceScreen(Screens.TAB_MAIN_SCREEN.name());
    }

    void onTabSearchClick() {
        router.replaceScreen(Screens.TAB_SEARCH_SCREEN.name());
    }

    void onTabMyOrdersClick() {
        router.replaceScreen(Screens.TAB_ORDERS_SCREEN.name());
    }

    void onTabNotificationsClick() {
        router.replaceScreen(Screens.TAB_NOTIFICATIONS_SCREEN.name());
    }

    void onTabProfileClick() {
        router.replaceScreen(Screens.TAB_PROFILE_SCREEN.name());
    }

    boolean isUserLogged() {
        return userInteractor.isUserLogged();
    }

}
