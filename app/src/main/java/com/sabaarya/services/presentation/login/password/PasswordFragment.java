package com.sabaarya.services.presentation.login.password;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

public class PasswordFragment extends MvpBaseFragment implements BackButtonListener, PasswordView {

    /*@BindView(R.id.email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.email)
    EditText email;*/

    @InjectPresenter
    PasswordPresenter presenter;

    @ProvidePresenter
    PasswordPresenter providePresenter() {
        return new PasswordPresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_password;
    }


    public static Fragment getNewInstance() {
        return new PasswordFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
