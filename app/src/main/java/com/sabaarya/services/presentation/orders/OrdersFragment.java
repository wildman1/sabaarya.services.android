package com.sabaarya.services.presentation.orders;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

public class OrdersFragment extends MvpBaseFragment implements OrdersView {

    @InjectPresenter
    OrdersPresenter presenter;

    @ProvidePresenter
    OrdersPresenter provideForwardPresenter() {
        return new OrdersPresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_orders;
    }

    public static OrdersFragment getNewInstance() {
        OrdersFragment fragment = new OrdersFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
