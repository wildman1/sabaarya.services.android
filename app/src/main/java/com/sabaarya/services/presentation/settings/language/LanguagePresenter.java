package com.sabaarya.services.presentation.settings.language;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class LanguagePresenter extends MvpBasePresenter<LanguageView> {

    @Inject
    AppDataInteractor dataInteractor;

    private final Router router;

    LanguagePresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setCurrentLanguage(dataInteractor.getLanguage());
    }

    void setLanguage(int language) {
        dataInteractor.setLanguage(language);
        dataInteractor.setCategoriesVersion(0);
        dataInteractor.saveAppVersion(null);
        getViewState().restartApp();
    }

    void onBackPressed() {
        router.exit();
    }
}
