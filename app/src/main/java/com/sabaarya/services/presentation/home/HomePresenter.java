package com.sabaarya.services.presentation.home;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.com.services.BuildConfig;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.data.network.models.HomeSlide;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.domain.interactors.HomeScreenInteractor;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.categories_list.CategoriesListFragment;
import com.sabaarya.services.presentation.category.CategoryFragment;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;

import static com.sabaarya.services.data.network.models.HomeSlide.TYPE_CATEGORY;
import static com.sabaarya.services.data.network.models.HomeSlide.TYPE_LINK;


@InjectViewState
public class HomePresenter extends MvpBasePresenter<HomeView> {

    private Router router;

    @Inject
    HomeScreenInteractor homeScreenInteractor;
    @Inject
    UserInteractor userInteractor;
    @Inject
    AppDataInteractor appDataInteractor;

    private AppVersion appVersion;

    HomePresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        appVersion = appDataInteractor.getAppVersionFromPrefs();
        if (appVersion != null) {
            getViewState().showHomeData(appVersion.slides);
            getCategories();
        }
        getViewState().setBalance(userInteractor.getUserBalance());
        getAppVersion(appVersion == null);
        getHomeData();
    }

    private void getHomeData() {
        homeScreenInteractor
                .getHomeData(userInteractor.getUserId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        res -> {
                            userInteractor.setUserBalance((float) (res.data.myGems + res.data.myPrices));
                            getViewState().setBalance(res.data.myGems + res.data.myPrices);
                        }
                        ,
                        throwable -> Log.e("HOME", "Failed to obtain user balance", throwable));
    }

    private void getAppVersion(boolean showLoading) {
        homeScreenInteractor
                .getAppVersion()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(showLoading))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        result -> {
                            getViewState().showHomeData(result.data.slides);
                            checkCategoriesUpdate(result.data);
                            if (BuildConfig.VERSION_CODE < result.data.appVersion) {
                                getViewState().showNewVersion();
                            }
                            this.appVersion = result.data;
                            appDataInteractor.saveAppVersion(appVersion);
                        },
                        throwable -> {
                            if (showLoading) {
                                getViewState().showServerError(throwable);
                            }
                        });
    }

    private void checkCategoriesUpdate(AppVersion version) {
        if (version.categoriesVersion > appDataInteractor.getCategoriesVersion()) {
            homeScreenInteractor
                    .getCategories(version.categoriesLink)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(res -> saveCategories(res, version.categoriesVersion), throwable -> getViewState().showServerError(throwable));
        }
    }

    private void saveCategories(List<Category> categories, int version) {
        homeScreenInteractor.saveCategories(categories)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> {
                    appDataInteractor.setCategoriesVersion(version);
                    getCategories();
                }, throwable -> getViewState().showServerError(throwable));
    }

    private void getCategories() {
        homeScreenInteractor.getCategoriesByParent(appVersion.catId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> getViewState().showCategories(appVersion.imageServer, res),
                        throwable -> getViewState().showServerError(throwable));
    }

    void onSlideClick(HomeSlide slide) {
        switch (slide.typeId) {
            case TYPE_LINK:
                getViewState().showLink(slide.link);
                break;
            case TYPE_CATEGORY:
                if (slide.catId != null) {
                    router.navigateTo(Screens.CATEGORY_SCREEN.name(), CategoryFragment.getBundle(slide.catId));
                }
                break;
        }
    }

    void onCategoryClicked(Category item) {
        if (item.hasChildren) {
            router.navigateTo(Screens.CATEGORIES_LIST_SCREEN.name(), CategoriesListFragment.getBundle(item.id));
        } else {
            router.navigateTo(Screens.CATEGORY_SCREEN.name(), CategoryFragment.getBundle(item.id));
        }
    }

    void onNewVersionClicked() {
        getViewState().showUpdateDialog(appVersion.updateTitle, appVersion.appLink);
    }

    void onSearchClicked() {
        router.navigateTo(Screens.SEARCH_SERVICES_SCREEN.name());
    }

}
