package com.sabaarya.services.presentation.main;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainView extends MvpBaseView {
    int MAIN_TAB_POSITION = 0;

    @StateStrategyType(OneExecutionStateStrategy.class)
    void exit();

}
