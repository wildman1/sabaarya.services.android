package com.sabaarya.services.presentation.categories_list;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

import java.util.List;

import butterknife.BindView;

public class CategoriesListFragment extends MvpBaseFragment implements BackButtonListener, CategoriesListView {

    private static final String CATEGORY_KEY = "category_key";

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    CategoriesListPresenter presenter;

    @ProvidePresenter
    CategoriesListPresenter provideConfirmationPresenter() {
        return new CategoriesListPresenter(getActivityRouter(), getArguments().getString(CATEGORY_KEY));
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_categories_list;
    }


    public static Fragment getNewInstance(Bundle arguments) {
        CategoriesListFragment fragment = new CategoriesListFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static Bundle getBundle(String categoryId) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_KEY, categoryId);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setNavigationOnClickListener(view -> presenter.onBackPressed());
        toolbar.setNavigationIcon(R.drawable.ic_back);
    }

    @Override
    public void setCategories(List<Category> categories, int startTab) {
        CategoriesPagerAdapter adapter = new CategoriesPagerAdapter(getChildFragmentManager(), categories);
        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);
        if (startTab > 0) {
            pager.setCurrentItem(startTab);
        }
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
