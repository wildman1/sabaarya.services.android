package com.sabaarya.services.presentation.login.confirmation_code;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.AuthCheck;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.domain.interactors.LoginScreenInteractor;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.login.profile_register.ProfileRegisterFragment;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class ConfirmationCodePresenter extends MvpBasePresenter<ConfirmationCodeView> {

    @Inject
    LoginScreenInteractor loginScreenInteractor;
    @Inject
    UserInteractor userInteractor;

    private final Router router;

    private String countryCode;
    private String username;

    ConfirmationCodePresenter(Router router, String countryCode, String username) {
        this.countryCode = countryCode;
        this.username = username;
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void next(String confirmationCode) {
        Flowable<Result<AuthCheck>> confirm = TextUtils.isEmpty(countryCode) ? loginScreenInteractor
                .checkEmail(confirmationCode, username) : loginScreenInteractor
                .checkPhone(confirmationCode, username);
        confirm
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        result -> {
                            switch (result.data.result) {
                                case AuthCheck.RESULT_WRONG:
                                    getViewState().showErrorMessage(result.description);
                                    break;
                                case AuthCheck.RESULT_LOGIN:
                                    userInteractor.saveUser(result.data);
                                    getViewState().restartApp();
                                    break;
                                case AuthCheck.RESULT_NEW:
                                    router.navigateTo(Screens.PROFILE_REGISTER_SCREEN.name(), ProfileRegisterFragment.getBundle(countryCode, username, confirmationCode));
                                    break;
                                case AuthCheck.RESULT_PASSWORD:
                                    router.navigateTo(Screens.PASSWORD_SCREEN.name());
                                    break;
                            }
                        },
                        throwable -> getViewState().showServerError(throwable));
    }

    void resendCode() {
        Flowable<Result<Boolean>> confirm = TextUtils.isEmpty(countryCode) ? loginScreenInteractor
                .confirmEmail(username) : loginScreenInteractor
                .confirmPhone(username);
        confirm
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        result -> getViewState().showErrorMessage(R.string.conf_code_resent),
                        throwable -> getViewState().showServerError(throwable));
    }

    void onBackPressed() {
        router.exit();
    }

}
