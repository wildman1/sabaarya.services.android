package com.sabaarya.services.presentation.login.password;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.LoginScreenInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class PasswordPresenter extends MvpBasePresenter<PasswordView> {

    @Inject
    LoginScreenInteractor loginScreenInteractor;

    private final Router router;

    PasswordPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void test() {
        //router.navigateTo(Screens.PROFILE_REGISTER_SCREEN.name());
    }


    void onBackPressed() {
        router.exit();
    }

}
