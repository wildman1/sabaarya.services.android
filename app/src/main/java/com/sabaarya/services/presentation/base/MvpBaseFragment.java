package com.sabaarya.services.presentation.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.RetrofitException;
import com.sabaarya.services.presentation.main.MainActivity;
import com.sabaarya.services.utils.LanguagesManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

public abstract class MvpBaseFragment extends MvpAppCompatFragment implements MvpBaseView {

    protected View rootView;

    @Nullable
    @BindView(R.id.progressBar)
    View progressBar;

    @Inject
    LanguagesManager languagesManager;

    private Toast toast;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected Unbinder unbinder;

    private boolean shouldNotAnimate = false;


    protected void addSub(Disposable subscription) {
        compositeDisposable.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagesManager.setLanguage(getContext());
    }

    public MvpBaseFragment() {
        App.INSTANCE.getAppComponent().inject(this);
    }

    protected abstract int setLayoutRes();

    protected void onPostCreateView() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(setLayoutRes(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        setupUI(rootView);
        onPostCreateView();
        return rootView;
    }


    protected void setupUI(View view) {
        if (!(view instanceof EditText) && view.getId() != R.id.clear) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard(getActivity());
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    protected void hideSoftKeyboard(Activity activity) {
        if (activity == null) return;
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    protected void showToast(String message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showToast(int message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showConnectionError() {
        showToast(getString(R.string.error_connection));
    }


    @Override
    public void showServerError(Throwable throwable) {
        throwable.printStackTrace();
        if (throwable instanceof RetrofitException && ((RetrofitException) throwable).errorMessageToUser != null)
            showToast(((RetrofitException) throwable).errorMessageToUser);
        else
            showToast(getString(R.string.error_server_failed));
    }

    @Override
    public void showInternalError() {
        showToast(getString(R.string.error_internal));
    }

    @Override
    public void showErrorMessage(String s) {
        showToast(s);
    }

    @Override
    public void showErrorMessage(int s) {
        showToast(s);
    }

    public void showLoading(boolean b) {
        if (progressBar != null)
            progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLink(String link) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return shouldNotAnimate ? AnimationUtils.loadAnimation(getActivity(), R.anim.none) : super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void setShouldNotAnimate() {
        this.shouldNotAnimate = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.shouldNotAnimate = false;
    }

    @Override
    public void restartApp() {
        MainActivity.restart(getContext());
    }

    protected Router getActivityRouter() {
        if (getActivity() == null) {
            return App.INSTANCE.getRouter();
        } else {
            return ((RouterProvider) getActivity()).getRouter();
        }
    }

}
