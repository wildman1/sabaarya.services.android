package com.sabaarya.services.presentation.login.send_confirmation;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface SendConfirmationCodeView extends MvpBaseView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showPhoneError();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showEmailError();

}
