package com.sabaarya.services.presentation.settings.main;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class SettingsPresenter extends MvpBasePresenter<SettingsView> {

    @Inject
    UserInteractor userInteractor;

    private final Router router;

    SettingsPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if (userInteractor.isUserLogged()) {
            getViewState().showLogout();
        }
    }

    void logout() {
        userInteractor.saveUser(null);
        getViewState().restartApp();
    }

    void openLanguageSettings() {
        router.navigateTo(Screens.LANGUAGE_SCREEN.name());
    }

    void onBackPressed() {
        router.exit();
    }
}
