package com.sabaarya.services.presentation.category;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.domain.interactors.CategoriesListInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class CategoryPresenter extends MvpBasePresenter<CategoryView> {

    @Inject
    CategoriesListInteractor categoriesInteractor;
    @Inject
    AppDataInteractor appDataInteractor;

    private final Router router;
    private String categoryId;

    CategoryPresenter(Router router, String categoryId) {
        App.INSTANCE.getAppComponent().inject(this);
        this.categoryId = categoryId;
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        categoriesInteractor.getCategoryById(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(res -> getViewState().showCategory(res, appDataInteractor.getAppVersionFromPrefs().imageServer),
                        throwable -> getViewState().showServerError(throwable));
    }

    void onBackPressed() {
        router.exit();
    }

}
