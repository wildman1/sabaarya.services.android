package com.sabaarya.services.presentation.category;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface CategoryView extends MvpBaseView {

    void showCategory(Category category, String imageServer);

}
