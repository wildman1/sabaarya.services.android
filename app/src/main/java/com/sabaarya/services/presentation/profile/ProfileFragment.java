package com.sabaarya.services.presentation.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.ProfileData;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.ImageUtils;
import com.sabaarya.services.utils.ViewUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileFragment extends MvpBaseFragment implements ProfileView {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.blocked_layout)
    View blockedLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @InjectPresenter
    ProfilePresenter presenter;
    @ProvidePresenter
    ProfilePresenter provideForwardPresenter() {
        return new ProfilePresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_profile;
    }

    public static ProfileFragment getNewInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.inflateMenu(R.menu.menu_settings_icon);
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.action_settings) {
                presenter.openSettings();
            }
            return true;
        });
    }

    @Override
    public void showProfileInfo(ProfileData profileData) {
        ImageUtils.loadImageProfile(logo, profileData.imageUrl);
        name.setText(profileData.companyName);
        ViewUtils.hideViews(blockedLayout);
    }

    @OnClick({R.id.edit_avatar, R.id.logo})
    void editAvatar() {

    }

    @Override
    public void showProfileError() {
        ViewUtils.showViews(blockedLayout);
    }
}
