package com.sabaarya.services.presentation.settings.language;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

import butterknife.BindView;

public class LanguageFragment extends MvpBaseFragment implements BackButtonListener, LanguageView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list)
    ListView languages;

    @InjectPresenter
    LanguagePresenter presenter;

    @ProvidePresenter
    LanguagePresenter providePresenter() {
        return new LanguagePresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_language;
    }


    public static Fragment getNewInstance() {
        return new LanguageFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        toolbar.setNavigationOnClickListener(view -> presenter.onBackPressed());
        toolbar.setNavigationIcon(R.drawable.ic_back);
        LanguagesAdapter adapter = new LanguagesAdapter(getContext());
        adapter.addAll(getResources().getStringArray(R.array.languages));
        languages.setAdapter(adapter);
        languages.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public void setCurrentLanguage(int language) {
        languages.setItemChecked(language, true);
        languages.setOnItemClickListener((parent, view, position, id) -> {
            if (position == language) {
                return;
            }
            presenter.setLanguage(position);
        });
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
