package com.sabaarya.services.presentation.category_children;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.sabaarya.com.services.R;

import butterknife.BindView;

public class CategoryStandaloneChildrenFragment extends CategoryChildrenFragment {

    private static final String CATEGORY_KEY = "category_key";
    private static final String CATEGORY_NAME = "category_name";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_stadalone_category_children;
    }

    public static Fragment getNewInstance(Bundle arguments) {
        CategoryStandaloneChildrenFragment fragment = new CategoryStandaloneChildrenFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static Bundle getBundle(String categoryId, String categoryName) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_KEY, categoryId);
        bundle.putString(CATEGORY_NAME, categoryName);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setNavigationOnClickListener(view -> presenter.onBackPressed());
        toolbar.setNavigationIcon(R.drawable.ic_back);
        title.setText(getArguments().getString(CATEGORY_NAME));
    }

}
