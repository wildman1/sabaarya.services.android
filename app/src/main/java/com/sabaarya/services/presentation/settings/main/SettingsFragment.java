package com.sabaarya.services.presentation.settings.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.ViewUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingsFragment extends MvpBaseFragment implements BackButtonListener, SettingsView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sign_out)
    TextView signOut;

    @InjectPresenter
    SettingsPresenter presenter;
    @ProvidePresenter
    SettingsPresenter providePresenter() {
        return new SettingsPresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_settings;
    }


    public static Fragment getNewInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setNavigationOnClickListener(view -> presenter.onBackPressed());
        toolbar.setNavigationIcon(R.drawable.ic_back);
    }

    @OnClick(R.id.sign_out)
    void logout() {
        presenter.logout();
    }

    @OnClick(R.id.language)
    void openLanguageSettings() {
        presenter.openLanguageSettings();
    }

    @Override
    public void showLogout() {
        ViewUtils.showViews(signOut);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
