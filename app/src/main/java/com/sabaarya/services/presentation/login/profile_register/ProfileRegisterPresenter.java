package com.sabaarya.services.presentation.login.profile_register;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.network.models.User;
import com.sabaarya.services.domain.interactors.LoginScreenInteractor;
import com.sabaarya.services.domain.interactors.UserInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class ProfileRegisterPresenter extends MvpBasePresenter<ProfileRegisterView> {

    @Inject
    LoginScreenInteractor loginScreenInteractor;
    @Inject
    UserInteractor userInteractor;

    private final Router router;

    private String countryCode;
    private String username;
    private String confirmationCode;

    ProfileRegisterPresenter(Router router, String countryCode, String username, String confirmationCode) {
        App.INSTANCE.getAppComponent().inject(this);
        this.countryCode = countryCode;
        this.username = username;
        this.confirmationCode = confirmationCode;
        this.router = router;
    }

    void next(String name) {
        if (TextUtils.isEmpty(name.trim())) {
            getViewState().showNameError();
        } else {
            Flowable<Result<User>> register = TextUtils.isEmpty(countryCode) ? loginScreenInteractor
                    .registerWithEmail(name, confirmationCode, username) : loginScreenInteractor
                    .registerWithPhone(name, confirmationCode, username);
            register
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(subscription -> getViewState().showLoading(true))
                    .doOnTerminate(() -> getViewState().showLoading(false))
                    .subscribe(
                            result -> {
                                userInteractor.saveUser(result.data);
                                getViewState().restartApp();
                            },
                            throwable -> getViewState().showServerError(throwable));
        }
    }


    void onBackPressed() {
        router.exit();
    }

}
