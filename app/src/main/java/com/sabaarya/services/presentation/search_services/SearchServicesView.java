package com.sabaarya.services.presentation.search_services;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.MvpBaseView;

import java.util.List;


@StateStrategyType(AddToEndSingleStrategy.class)
interface SearchServicesView extends MvpBaseView {

    void showCategories(String imageServer, List<Category> categories);

}
