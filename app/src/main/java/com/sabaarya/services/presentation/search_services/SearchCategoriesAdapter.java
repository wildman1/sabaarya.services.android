package com.sabaarya.services.presentation.search_services;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class SearchCategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Category> dataset = new ArrayList<>();
    private PublishSubject<Category> publishSubject = PublishSubject.create();

    private String imageServer;

    void setDataset(String imageServer, List<Category> dataset) {
        this.imageServer = imageServer;
        if (dataset == null) {
            this.dataset.clear();
        } else {
            this.dataset = dataset;
        }
        notifyDataSetChanged();
    }

    Observable<Category> clicks() {
        return publishSubject;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SearchCategoriesAdapter.CategoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_child, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CategoryViewHolder) holder).bind(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.image)
        ImageView image;

        Category category;

        CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> publishSubject.onNext(category));
        }

        public void bind(Category category) {
            this.category = category;
            name.setText(category.title);
            ImageUtils.loadImageCenterCrop(image, imageServer + category.image);
        }
    }

}
