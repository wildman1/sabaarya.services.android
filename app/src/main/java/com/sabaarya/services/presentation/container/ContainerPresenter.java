package com.sabaarya.services.presentation.container;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class ContainerPresenter extends MvpBasePresenter<ContainerView> {
    private Router router;

    ContainerPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void startScreen(String screenName, Bundle data) {
        router.replaceScreen(screenName, data);
    }

    void onBackPressed() {
        router.exit();
    }

}
