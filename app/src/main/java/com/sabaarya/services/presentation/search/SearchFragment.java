package com.sabaarya.services.presentation.search;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

public class SearchFragment extends MvpBaseFragment implements SearchView {

    @InjectPresenter
    SearchPresenter presenter;

    @ProvidePresenter
    SearchPresenter provideForwardPresenter() {
        return new SearchPresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_search;
    }

    public static SearchFragment getNewInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
