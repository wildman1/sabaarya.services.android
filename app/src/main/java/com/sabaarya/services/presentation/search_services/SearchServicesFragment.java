package com.sabaarya.services.presentation.search_services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.ViewUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.reactivex.disposables.Disposable;

public class SearchServicesFragment extends MvpBaseFragment implements SearchServicesView {

    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.clear)
    ImageView clear;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @InjectPresenter
    SearchServicesPresenter presenter;
    @ProvidePresenter
    SearchServicesPresenter provideForwardPresenter() {
        return new SearchServicesPresenter(getActivityRouter());
    }

    private SearchCategoriesAdapter adapter;
    private Disposable disposable;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_search_services;
    }

    public static SearchServicesFragment getNewInstance() {
        SearchServicesFragment fragment = new SearchServicesFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        adapter = new SearchCategoriesAdapter();
        if (disposable != null) disposable.dispose();
        disposable = adapter.clicks().subscribe(item -> {
            hideSoftKeyboard(getActivity());
            presenter.onCategoryClicked(item);
        });
        addSub(disposable);
        recyclerView.setAdapter(adapter);
        search.setOnEditorActionListener((v1, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard(getActivity());
            }
            return false;
        });
    }

    @Override
    public void showCategories(String imageServer, List<Category> categories) {
        adapter.setDataset(imageServer, categories);
    }

    @OnTextChanged(value = R.id.search, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onTexted() {
        presenter.performSearch(search.getText().toString());
        if (search.getText().toString().trim().length() > 0) {
            ViewUtils.showViews(clear);
        } else {
            ViewUtils.hideViews(clear);
        }
    }

    @OnClick(R.id.clear)
    public void onClickRemove() {
        search.setText("");
    }

}
