package com.sabaarya.services.presentation.base;

public interface BackButtonListener {
    boolean onBackPressed();
}
