package com.sabaarya.services.presentation.base;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.RetrofitException;
import com.sabaarya.services.presentation.main.MainActivity;
import com.sabaarya.services.utils.LanguagesManager;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class MvpBaseActivity extends MvpAppCompatActivity implements MvpBaseView {

    @Inject
    LanguagesManager languagesManager;

    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected View rootView;

    private Toast toast;

    protected void addSub(Disposable subscription) {
        compositeDisposable.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    protected abstract int getLayoutRes();

    public MvpBaseActivity() {
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagesManager.setLanguage(this);
        if (getLayoutRes() > 0) {
            setContentView(getLayoutRes());
            rootView = findViewById(android.R.id.content);
            ButterKnife.bind(this, rootView);
        }
        if (savedInstanceState == null) {
            Fragment fragment = getContentFragment();
            if (fragment != null) {
                replaceContentFragmentWith(fragment);
            }
        }
    }

    @Nullable
    protected Fragment getContentFragment() {
        return null;
    }

    @IdRes
    protected int getIdForContentFragment() {
        return android.R.id.content;
    }

    protected void replaceContentFragmentWith(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(getIdForContentFragment(), fragment)
                .commitNow();
    }

        @Override
    protected void attachBaseContext(Context newBase) {
        if (languagesManager.isFontOverriden()) {
            super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    protected void showToast(String message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showToast(int message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showLoading(boolean loading) {

    }

    @Override
    public void restartApp() {
        MainActivity.restart(this);
    }

    @Override
    public void showConnectionError() {
        showToast(getString(R.string.error_connection));
    }

    @Override
    public void showServerError(Throwable throwable) {
        throwable.printStackTrace();
        if (throwable instanceof RetrofitException && ((RetrofitException) throwable).errorMessageToUser != null)
            showToast(((RetrofitException) throwable).errorMessageToUser);
        else
            showToast(getString(R.string.error_server_failed));
    }

    @Override
    public void showErrorMessage(String s) {
        showToast(s);
    }

    @Override
    public void showLink(String link) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
    }

    @Override
    public void showInternalError() {
        showToast(getString(R.string.error_internal));
    }

    @Override
    public void showErrorMessage(int errorConnection) {
        showToast(errorConnection);
    }

}
