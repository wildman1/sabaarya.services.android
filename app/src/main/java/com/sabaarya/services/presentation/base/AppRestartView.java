package com.sabaarya.services.presentation.base;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface AppRestartView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void restartApp();
}
