package com.sabaarya.services.presentation.login.profile_register;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.view.AfterTextWatcher;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileRegisterFragment extends MvpBaseFragment implements BackButtonListener, ProfileRegisterView {

    private static final String COUNTRY_CODE_KEY = "country_code";
    private static final String USERNAME_KEY = "username";
    private static final String CONFIRMATION_CODE_KEY = "confirmation_code";

    @BindView(R.id.name_layout)
    TextInputLayout nameLayout;
    @BindView(R.id.name)
    EditText name;

    @InjectPresenter
    ProfileRegisterPresenter presenter;

    @ProvidePresenter
    ProfileRegisterPresenter provideConfirmationPresenter() {
        return new ProfileRegisterPresenter(getActivityRouter(),
                getArguments().getString(COUNTRY_CODE_KEY), getArguments().getString(USERNAME_KEY), getArguments().getString(CONFIRMATION_CODE_KEY));
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_profile_register;
    }


    public static Fragment getNewInstance(Bundle arguments) {
        ProfileRegisterFragment fragment = new ProfileRegisterFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static Bundle getBundle(String countryCode, String userName, String confirmationCode) {
        Bundle bundle = new Bundle();
        bundle.putString(COUNTRY_CODE_KEY, countryCode);
        bundle.putString(USERNAME_KEY, userName);
        bundle.putString(CONFIRMATION_CODE_KEY, confirmationCode);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        name.addTextChangedListener(new AfterTextWatcher(s -> nameLayout.setError(null)));
    }

    @OnClick(R.id.next)
    void onClickNext() {
        presenter.next(name.getText().toString());
    }

    @Override
    public void showNameError() {
        nameLayout.setError(getString(R.string.enter_your_name_error));
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
