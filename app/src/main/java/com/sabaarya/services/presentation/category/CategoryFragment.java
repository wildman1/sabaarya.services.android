package com.sabaarya.services.presentation.category;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.ImageUtils;

import butterknife.BindView;

public class CategoryFragment extends MvpBaseFragment implements BackButtonListener, CategoryView {

    private static final String CATEGORY_KEY = "category_key";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image)
    ImageView imageView;
    @BindView(R.id.title)
    TextView title;


    @InjectPresenter
    CategoryPresenter presenter;
    @ProvidePresenter
    CategoryPresenter provideConfirmationPresenter() {
        return new CategoryPresenter(getActivityRouter(), getArguments().getString(CATEGORY_KEY));
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_category;
    }


    public static Fragment getNewInstance(Bundle arguments) {
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public static Bundle getBundle(String categoryId) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_KEY, categoryId);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setNavigationOnClickListener(view -> presenter.onBackPressed());
        toolbar.setNavigationIcon(R.drawable.ic_back);
    }

    @Override
    public void showCategory(Category category, String imageServer) {
        title.setText(category.title);
        ImageUtils.loadImageCenterCrop(imageView, imageServer + category.image);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
