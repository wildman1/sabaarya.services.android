package com.sabaarya.services.presentation.base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MvpBaseView extends SimpleErrorView, LoadingView, MvpView, AppRestartView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showLink(String link);

}
