package com.sabaarya.services.presentation.settings.language;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sabaarya.com.services.R;


public class LanguagesAdapter extends ArrayAdapter<String> {

    private final LayoutInflater inflater;

    LanguagesAdapter(final Context context) {
        super(context, 0);
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_language, parent, false);
        } else {
            view = convertView;
        }
        TextView item = view.findViewById(R.id.name);
        item.setText(getItem(position));
        return view;
    }

}
