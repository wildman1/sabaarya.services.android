package com.sabaarya.services.presentation.settings.language;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface LanguageView extends MvpBaseView {

    void setCurrentLanguage(int language);

}
