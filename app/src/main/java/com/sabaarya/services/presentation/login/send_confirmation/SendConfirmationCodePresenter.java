package com.sabaarya.services.presentation.login.send_confirmation;

import android.text.TextUtils;
import android.util.Patterns;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.domain.interactors.LoginScreenInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.login.confirmation_code.ConfirmationCodeFragment;
import com.sabaarya.services.utils.Values;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class SendConfirmationCodePresenter extends MvpBasePresenter<SendConfirmationCodeView> {

    @Inject
    LoginScreenInteractor loginScreenInteractor;

    private final Router router;

    SendConfirmationCodePresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void confirmPhone(String phone, String formatted) {
        if (TextUtils.isEmpty(phone)) {
            getViewState().showEmailError();
        } else {
            loginScreenInteractor
                    .confirmPhone(phone)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(subscription -> getViewState().showLoading(true))
                    .doOnTerminate(() -> getViewState().showLoading(false))
                    .subscribe(
                            result -> router.navigateTo(Screens.CONFIRMATION_CODE_SCREEN.name(), ConfirmationCodeFragment.getBundle(Values.COUNTRY_CODE, phone, formatted)),
                            throwable -> getViewState().showServerError(throwable));
        }
    }

    void confirmEmail(String email) {
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginScreenInteractor
                    .confirmEmail(email)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(subscription -> getViewState().showLoading(true))
                    .doOnTerminate(() -> getViewState().showLoading(false))
                    .subscribe(
                            result -> router.navigateTo(Screens.CONFIRMATION_CODE_SCREEN.name(), ConfirmationCodeFragment.getBundle(email)),
                            throwable -> getViewState().showServerError(throwable));
        } else {
            getViewState().showEmailError();
        }
    }

    void openSettings() {
        router.navigateTo(Screens.SETTINGS_SCREEN.name());
    }

    void onBackPressed() {
        router.exit();
    }
}
