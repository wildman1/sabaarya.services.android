package com.sabaarya.services.presentation.search_services;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.domain.interactors.CategoriesListInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.category.CategoryFragment;
import com.sabaarya.services.presentation.category_children.CategoryStandaloneChildrenFragment;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class SearchServicesPresenter extends MvpBasePresenter<SearchServicesView> {

    @Inject
    CategoriesListInteractor categoriesInteractor;
    @Inject
    AppDataInteractor appDataInteractor;

    private Router router;

    SearchServicesPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    void performSearch(String query) {
        if (TextUtils.isEmpty(query.trim())) {
            getViewState().showCategories(null, null);
        } else {
            categoriesInteractor.searchCategoriesByTitle(query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(subscription -> getViewState().showLoading(true))
                    .doOnTerminate(() -> getViewState().showLoading(false))
                    .subscribe(res -> getViewState().showCategories(appDataInteractor.getAppVersionFromPrefs().imageServer, res),
                            throwable -> getViewState().showServerError(throwable));
        }
    }

    void onCategoryClicked(Category item) {
        if (item.hasChildren) {
            router.navigateTo(Screens.CATEGORY_STANDALONE_CHILDREN_SCREEN.name(), CategoryStandaloneChildrenFragment.getBundle(item.id, item.title));
        } else {
            router.navigateTo(Screens.CATEGORY_SCREEN.name(), CategoryFragment.getBundle(item.id));
        }
    }

}
