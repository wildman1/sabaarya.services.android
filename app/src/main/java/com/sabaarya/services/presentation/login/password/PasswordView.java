package com.sabaarya.services.presentation.login.password;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface PasswordView extends MvpBaseView {

    /*@StateStrategyType(OneExecutionStateStrategy.class)
    void showPhoneError();*/

}
