package com.sabaarya.services.presentation.login.confirmation_code;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
interface ConfirmationCodeView extends MvpBaseView {

    /*@StateStrategyType(OneExecutionStateStrategy.class)
    void showPhoneError();*/

}
