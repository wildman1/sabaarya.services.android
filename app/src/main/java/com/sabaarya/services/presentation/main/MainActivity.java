package com.sabaarya.services.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.App;
import com.sabaarya.services.presentation.base.MvpBaseActivity;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.presentation.base.RouterProvider;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.container.ContainerActivity;
import com.sabaarya.services.presentation.home.HomeFragment;
import com.sabaarya.services.presentation.login.send_confirmation.SendConfirmationCodeFragment;
import com.sabaarya.services.presentation.notifications.NotificationsFragment;
import com.sabaarya.services.presentation.orders.OrdersFragment;
import com.sabaarya.services.presentation.profile.ProfileFragment;
import com.sabaarya.services.presentation.search.SearchFragment;
import com.sabaarya.services.utils.BottomNavigationViewHelper;

import javax.inject.Inject;

import butterknife.BindView;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;


public class MainActivity extends MvpBaseActivity implements MainView, RouterProvider {

    private MvpBaseFragment homeTabFragment;
    private MvpBaseFragment searchTabFragment;
    private MvpBaseFragment orderTabFragment;
    private MvpBaseFragment notificationsTabFragment;
    private MvpBaseFragment profileTabFragment;

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    public MainPresenter presenter;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;


    @ProvidePresenter
    public MainPresenter createMainPresenter() {
        return new MainPresenter(router);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        initViews();
        initContainers();

        if (savedInstanceState == null) {
            bottomNavigationView.setSelectedItemId(MAIN_TAB_POSITION);
            presenter.onTabMainClick();
        }
    }

    private void initViews() {
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_main:
                    presenter.onTabMainClick();
                    break;
                case R.id.action_search:
                    presenter.onTabSearchClick();
                    break;
                case R.id.action_order:
                    presenter.onTabMyOrdersClick();
                    break;
                case R.id.action_notifications:
                    presenter.onTabNotificationsClick();
                    break;
                case R.id.action_profile:
                    presenter.onTabProfileClick();
                    break;
            }
            return true;
        });
    }

    private void initContainers() {
        FragmentManager fm = getSupportFragmentManager();
        homeTabFragment = (MvpBaseFragment) fm.findFragmentByTag("MAIN");
        if (homeTabFragment == null) {
            homeTabFragment = HomeFragment.getNewInstance();
            fm.beginTransaction()
                    .add(R.id.container, homeTabFragment, "MAIN")
                    .detach(homeTabFragment).commitNow();
        }

        searchTabFragment = (MvpBaseFragment) fm.findFragmentByTag("SEARCH");
        if (searchTabFragment == null) {
            searchTabFragment = SearchFragment.getNewInstance();
            fm.beginTransaction()
                    .add(R.id.container, searchTabFragment, "SEARCH")
                    .detach(searchTabFragment).commitNow();
        }

        orderTabFragment = (MvpBaseFragment) fm.findFragmentByTag("ORDERS");
        if (orderTabFragment == null) {
            orderTabFragment = OrdersFragment.getNewInstance();
            fm.beginTransaction()
                    .add(R.id.container, orderTabFragment, "ORDERS")
                    .detach(orderTabFragment).commitNow();
        }

        notificationsTabFragment = (MvpBaseFragment) fm.findFragmentByTag("NOTIFICATIONS");
        if (notificationsTabFragment == null) {
            notificationsTabFragment = NotificationsFragment.getNewInstance();
            fm.beginTransaction()
                    .add(R.id.container, notificationsTabFragment, "NOTIFICATIONS")
                    .detach(notificationsTabFragment).commitNow();
        }

        profileTabFragment = (MvpBaseFragment) fm.findFragmentByTag("PROFILE");
        if (profileTabFragment == null) {
            profileTabFragment = (MvpBaseFragment) (presenter.isUserLogged() ? ProfileFragment.getNewInstance() : SendConfirmationCodeFragment.getNewInstance());
            fm.beginTransaction()
                    .add(R.id.container, profileTabFragment, "PROFILE")
                    .detach(profileTabFragment).commitNow();
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    private Navigator navigator = new Navigator() {

        @Override
        public void applyCommands(Command[] commands) {
            Command command = commands[0];
            if (command instanceof Back) {
                presenter.onBackPressed();
            } else if (command instanceof SystemMessage) {
                Toast.makeText(MainActivity.this, ((SystemMessage) command).getMessage(), Toast.LENGTH_SHORT).show();
            } else if (command instanceof Replace) {
                FragmentManager fm = getSupportFragmentManager();
                switch (Screens.valueOf(((Replace) command).getScreenKey())) {
                    case TAB_MAIN_SCREEN:
                        homeTabFragment.setShouldNotAnimate();
                        searchTabFragment.setShouldNotAnimate();
                        orderTabFragment.setShouldNotAnimate();
                        notificationsTabFragment.setShouldNotAnimate();
                        profileTabFragment.setShouldNotAnimate();
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        R.anim.fade_in,
                                        R.anim.fade_out,
                                        R.anim.fade_in,
                                        R.anim.fade_out)
                                .detach(searchTabFragment)
                                .detach(orderTabFragment)
                                .detach(notificationsTabFragment)
                                .detach(profileTabFragment)
                                .attach(homeTabFragment)
                                .commit();
                        break;
                    case TAB_SEARCH_SCREEN:
                        homeTabFragment.setShouldNotAnimate();
                        searchTabFragment.setShouldNotAnimate();
                        orderTabFragment.setShouldNotAnimate();
                        notificationsTabFragment.setShouldNotAnimate();
                        profileTabFragment.setShouldNotAnimate();
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        R.anim.fade_in,
                                        R.anim.fade_out,
                                        R.anim.fade_in,
                                        R.anim.fade_out)
                                .detach(homeTabFragment)
                                .detach(orderTabFragment)
                                .detach(notificationsTabFragment)
                                .detach(profileTabFragment)
                                .attach(searchTabFragment)
                                .commit();

                        break;
                    case TAB_ORDERS_SCREEN:
                        homeTabFragment.setShouldNotAnimate();
                        searchTabFragment.setShouldNotAnimate();
                        orderTabFragment.setShouldNotAnimate();
                        notificationsTabFragment.setShouldNotAnimate();
                        profileTabFragment.setShouldNotAnimate();
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        R.anim.fade_in,
                                        R.anim.fade_out,
                                        R.anim.fade_in,
                                        R.anim.fade_out)
                                .detach(homeTabFragment)
                                .detach(searchTabFragment)
                                .detach(notificationsTabFragment)
                                .detach(profileTabFragment)
                                .attach(orderTabFragment)
                                .commit();
                        break;
                    case TAB_NOTIFICATIONS_SCREEN:
                        homeTabFragment.setShouldNotAnimate();
                        searchTabFragment.setShouldNotAnimate();
                        orderTabFragment.setShouldNotAnimate();
                        notificationsTabFragment.setShouldNotAnimate();
                        profileTabFragment.setShouldNotAnimate();
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        R.anim.fade_in,
                                        R.anim.fade_out,
                                        R.anim.fade_in,
                                        R.anim.fade_out)
                                .detach(homeTabFragment)
                                .detach(searchTabFragment)
                                .detach(orderTabFragment)
                                .detach(profileTabFragment)
                                .attach(notificationsTabFragment)
                                .commit();
                        break;
                    case TAB_PROFILE_SCREEN:
                        homeTabFragment.setShouldNotAnimate();
                        searchTabFragment.setShouldNotAnimate();
                        orderTabFragment.setShouldNotAnimate();
                        notificationsTabFragment.setShouldNotAnimate();
                        profileTabFragment.setShouldNotAnimate();
                        fm.beginTransaction()
                                .setCustomAnimations(
                                        R.anim.fade_in,
                                        R.anim.fade_out,
                                        R.anim.fade_in,
                                        R.anim.fade_out)
                                .detach(homeTabFragment)
                                .detach(searchTabFragment)
                                .detach(orderTabFragment)
                                .detach(notificationsTabFragment)
                                .attach(profileTabFragment)
                                .commit();
                        break;
                }
            } else if (command instanceof Forward) {
                ContainerActivity.start(MainActivity.this, ((Forward) command).getScreenKey(), (Bundle) ((Forward) command).getTransitionData());
            }
        }

    };

    /*public void selectTab(int itemId) {
        bottomNavigationView.setSelectedItemId(itemId);
    }*/

    @Override
    public Router getRouter() {
        return router;
    }

    @Override
    public void exit() {
        finish();
    }

    public static void start(Context context) {
        context.startActivity(intent(context));
    }

    public static void restart(Context context) {
        context.startActivity(intent(context).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @NonNull
    private static Intent intent(Context context) {
        return new Intent(context, MainActivity.class);
    }


}
