package com.sabaarya.services.presentation.categories_list;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.category_children.CategoryChildrenFragment;

import java.util.List;


public class CategoriesPagerAdapter extends FragmentStatePagerAdapter {

    private List<Category> categories;

    CategoriesPagerAdapter(FragmentManager fm, List<Category> categories) {
        super(fm);
        this.categories = categories;
    }

    @Override
    public Fragment getItem(int position) {
        return CategoryChildrenFragment.getNewInstance(CategoryChildrenFragment.getBundle(categories.get(position).id));
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).title;
    }

}
