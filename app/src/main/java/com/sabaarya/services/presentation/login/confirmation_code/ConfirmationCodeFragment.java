package com.sabaarya.services.presentation.login.confirmation_code;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.BackButtonListener;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmationCodeFragment extends MvpBaseFragment implements BackButtonListener, ConfirmationCodeView {

    private static final String USERNAME = "username";
    private static final String COUNTRY_CODE = "country_code";
    private static final String FORMATTED = "formatted";

    @BindView(R.id.code_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.confirmation_code)
    TextView confirmationCode;

    @InjectPresenter
    ConfirmationCodePresenter presenter;

    @ProvidePresenter
    ConfirmationCodePresenter provideConfirmationPresenter() {
        return new ConfirmationCodePresenter(getActivityRouter(), getArguments().getString(COUNTRY_CODE), getArguments().getString(USERNAME));
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_confirmation_code;
    }


    public static Fragment getNewInstance(Bundle bundle) {
        ConfirmationCodeFragment fragment = new ConfirmationCodeFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Bundle getBundle(String countryCode, String username, String formatted) {
        Bundle bundle = new Bundle();
        bundle.putString(COUNTRY_CODE, countryCode);
        bundle.putString(USERNAME, username);
        bundle.putString(FORMATTED, formatted);
        return bundle;
    }

    public static Bundle getBundle(String email) {
        Bundle bundle = new Bundle();
        bundle.putString(USERNAME, email);
        return bundle;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        confirmationCode.setText(getString(R.string.enter_code_text,
                TextUtils.isEmpty(getArguments().getString(COUNTRY_CODE)) ? getArguments().getString(USERNAME) : getArguments().getString(FORMATTED)));
    }

    @OnClick(R.id.next)
    void next() {
        presenter.next(code.getText().toString());
    }

    @OnClick(R.id.resend_code)
    void resendCode() {
        presenter.resendCode();
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

}
