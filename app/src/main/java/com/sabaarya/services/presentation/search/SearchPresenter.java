package com.sabaarya.services.presentation.search;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import ru.terrakok.cicerone.Router;


@InjectViewState
public class SearchPresenter extends MvpBasePresenter<SearchView> {
    private Router router;

    SearchPresenter(Router router) {
        this.router = router;
    }

}
