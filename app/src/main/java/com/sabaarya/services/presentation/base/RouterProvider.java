package com.sabaarya.services.presentation.base;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {
    Router getRouter();
}
