package com.sabaarya.services.presentation.categories_list;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.domain.interactors.CategoriesListInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class CategoriesListPresenter extends MvpBasePresenter<CategoriesListView> {

    @Inject
    CategoriesListInteractor categoriesListInteractor;
    @Inject
    AppDataInteractor appDataInteractor;

    private final Router router;

    private String categoryId;

    CategoriesListPresenter(Router router, String categoryId) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
        this.categoryId = categoryId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        categoriesListInteractor.getCategoriesWithChildrenByParent(appDataInteractor.getAppVersionFromPrefs().catId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(res -> getViewState().setCategories(res, res.indexOf(new Category(categoryId))),
                        throwable -> getViewState().showServerError(throwable));
    }

    void onBackPressed() {
        router.exit();
    }

}
