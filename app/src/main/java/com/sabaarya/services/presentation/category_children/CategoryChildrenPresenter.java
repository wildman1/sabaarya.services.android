package com.sabaarya.services.presentation.category_children;

import com.arellomobile.mvp.InjectViewState;
import com.sabaarya.services.App;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.domain.interactors.AppDataInteractor;
import com.sabaarya.services.domain.interactors.CategoriesListInteractor;
import com.sabaarya.services.presentation.base.MvpBasePresenter;
import com.sabaarya.services.presentation.base.Screens;
import com.sabaarya.services.presentation.category.CategoryFragment;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.terrakok.cicerone.Router;


@InjectViewState
public class CategoryChildrenPresenter extends MvpBasePresenter<CategoryChildrenView> {

    @Inject
    CategoriesListInteractor categoriesInteractor;
    @Inject
    AppDataInteractor appDataInteractor;

    private final Router router;

    private String categoryId;

    CategoryChildrenPresenter(Router router, String categoryId) {
        App.INSTANCE.getAppComponent().inject(this);
        this.categoryId = categoryId;
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        categoriesInteractor.getCategoriesByParent(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(res -> getViewState().showCategories(appDataInteractor.getAppVersionFromPrefs().imageServer, res),
                        throwable -> getViewState().showServerError(throwable));
    }

    void onBackPressed() {
        router.exit();
    }

    void onCategoryClicked(Category item) {
        router.navigateTo(Screens.CATEGORY_SCREEN.name(), CategoryFragment.getBundle(item.id));
    }
}
