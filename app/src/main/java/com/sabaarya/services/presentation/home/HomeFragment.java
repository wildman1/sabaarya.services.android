package com.sabaarya.services.presentation.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.sabaarya.com.services.R;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.data.network.models.HomeSlide;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.utils.ViewUtils;
import com.sabaarya.services.utils.view.recycler.SpacesItemDecoration;

import java.text.NumberFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

public class HomeFragment extends MvpBaseFragment implements HomeView, BaseSliderView.OnSliderClickListener {

    @BindView(R.id.slider)
    SliderLayout slider;
    @BindView(R.id.custom_indicator)
    PagerIndicator indicator;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.my_balance)
    TextView myBalance;
    @BindView(R.id.new_version)
    TextView newVersion;

    @InjectPresenter
    HomePresenter presenter;

    @ProvidePresenter
    HomePresenter provideForwardPresenter() {
        return new HomePresenter(getActivityRouter());
    }

    private HomeCategoriesAdapter adapter;
    private Disposable disposable;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_home;
    }

    public static HomeFragment getNewInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new HomeCategoriesAdapter();
        if (disposable != null) disposable.dispose();
        disposable = adapter.clicks().subscribe(item -> presenter.onCategoryClicked(item));
        addSub(disposable);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new SpacesItemDecoration(0));
    }

    @OnClick(R.id.new_version)
    void onNewVersionClicked() {
        presenter.onNewVersionClicked();
    }

    @OnClick(R.id.search)
    void onSearchClicked() {
        presenter.onSearchClicked();
    }

    @Override
    public void showHomeData(List<HomeSlide> slides) {
        setUpSlider(slides);
    }

    @Override
    public void showCategories(String imageServer, List<Category> categories) {
        adapter.setDataset(imageServer, categories);
    }

    @Override
    public void setBalance(double balance) {
        myBalance.setText(getString(R.string.balance, NumberFormat.getInstance().format(balance)));
    }

    @Override
    public void showNewVersion() {
        ViewUtils.showViews(newVersion);
    }

    @Override
    public void showUpdateDialog(String title, String link) {
        new NewVersionDialog(getContext(), title, link).show();
    }

    private void setUpSlider(List<HomeSlide> slides) {
        slider.removeAllSliders();
        for (HomeSlide slide : slides) {
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            textSliderView
                    .image(slide.image)
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putParcelable("slide", slide);
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setCustomIndicator(indicator);
        slider.setDuration(8_000);
        slider.moveNextPosition(false);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        presenter.onSlideClick(slider.getBundle().getParcelable("slide"));
    }

}
