package com.sabaarya.services.presentation.categories_list;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.presentation.base.MvpBaseView;

import java.util.List;


@StateStrategyType(AddToEndSingleStrategy.class)
interface CategoriesListView extends MvpBaseView {

    void setCategories(List<Category> categories, int startTab);

}
