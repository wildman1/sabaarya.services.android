package com.sabaarya.services.presentation.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.sabaarya.com.services.R;
import com.sabaarya.services.presentation.base.MvpBaseFragment;

public class NotificationsFragment extends MvpBaseFragment implements NotificationsView {

    @InjectPresenter
    NotificationsPresenter presenter;

    @ProvidePresenter
    NotificationsPresenter provideForwardPresenter() {
        return new NotificationsPresenter(getActivityRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_notifications;
    }

    public static NotificationsFragment getNewInstance() {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
