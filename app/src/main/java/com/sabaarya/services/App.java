package com.sabaarya.services;


import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.sabaarya.com.services.BuildConfig;
import com.sabaarya.com.services.R;
import com.sabaarya.services.dagger.AppComponent;
import com.sabaarya.services.dagger.DaggerAppComponent;
import com.sabaarya.services.dagger.module.ApplicationModule;

import io.fabric.sdk.android.Fabric;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Router;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class App extends MultiDexApplication {
    public static App INSTANCE;
    private AppComponent appComponent;

    private Cicerone<Router> cicerone;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("IRANSansMobile.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        cicerone = Cicerone.create();
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return appComponent;
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }

}
