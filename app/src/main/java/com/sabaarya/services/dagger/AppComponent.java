package com.sabaarya.services.dagger;

import com.sabaarya.services.dagger.module.ApplicationModule;
import com.sabaarya.services.dagger.module.DatabaseModule;
import com.sabaarya.services.dagger.module.NavigationModule;
import com.sabaarya.services.dagger.module.NetworkModule;
import com.sabaarya.services.presentation.base.MvpBaseActivity;
import com.sabaarya.services.presentation.base.MvpBaseFragment;
import com.sabaarya.services.presentation.categories_list.CategoriesListPresenter;
import com.sabaarya.services.presentation.category.CategoryPresenter;
import com.sabaarya.services.presentation.category_children.CategoryChildrenPresenter;
import com.sabaarya.services.presentation.container.ContainerActivity;
import com.sabaarya.services.presentation.container.ContainerPresenter;
import com.sabaarya.services.presentation.home.HomePresenter;
import com.sabaarya.services.presentation.login.confirmation_code.ConfirmationCodePresenter;
import com.sabaarya.services.presentation.login.password.PasswordPresenter;
import com.sabaarya.services.presentation.login.profile_register.ProfileRegisterPresenter;
import com.sabaarya.services.presentation.login.send_confirmation.SendConfirmationCodePresenter;
import com.sabaarya.services.presentation.main.MainActivity;
import com.sabaarya.services.presentation.main.MainPresenter;
import com.sabaarya.services.presentation.profile.ProfilePresenter;
import com.sabaarya.services.presentation.search_services.SearchServicesPresenter;
import com.sabaarya.services.presentation.settings.language.LanguagePresenter;
import com.sabaarya.services.presentation.settings.main.SettingsPresenter;
import com.sabaarya.services.utils.views.SansTabLayout;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        NavigationModule.class,
        NetworkModule.class,
        DatabaseModule.class,
        ApplicationModule.class
})
public interface AppComponent {

    void inject(SendConfirmationCodePresenter presenter);

    void inject(MainPresenter presenter);

    void inject(MainActivity mainActivity);

    void inject(HomePresenter presenter);

    void inject(ContainerPresenter presenter);

    void inject(ContainerActivity containerActivity);

    void inject(ConfirmationCodePresenter presenter);

    void inject(ProfileRegisterPresenter presenter);

    void inject(PasswordPresenter presenter);

    void inject(ProfilePresenter presenter);

    void inject(CategoryPresenter presenter);

    void inject(CategoriesListPresenter presenter);

    void inject(CategoryChildrenPresenter presenter);

    void inject(SearchServicesPresenter presenter);

    void inject(MvpBaseFragment mvpBaseFragment);

    void inject(MvpBaseActivity mvpBaseActivity);

    void inject(LanguagePresenter presenter);

    void inject(SettingsPresenter presenter);

    void inject(SansTabLayout tabLayout);
}
