package com.sabaarya.services.dagger.module;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.sabaarya.services.data.preference.Preferences;
import com.sabaarya.services.utils.NetworkAvailability;
import com.sabaarya.services.utils.ResourceProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences sharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    NetworkAvailability networkAvailability(Application application) {
        return NetworkAvailability.getInstance(application);
    }

    @Provides
    @Singleton
    NotificationManager notificationManager(Application application) {
        return (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Provides
    @Singleton
    LocalBroadcastManager localBroadcastManager(Application application) {
        return LocalBroadcastManager.getInstance(application);
    }

    @Provides
    @Singleton
    ResourceProvider resourceProvider(Application application) {
        return new ResourceProvider(application);
    }

    @Provides
    @Singleton
    Preferences preferences(SharedPreferences sharedPreferences) {
        return new Preferences(sharedPreferences);
    }
}
