package com.sabaarya.services.dagger.module;

import com.sabaarya.com.services.BuildConfig;
import com.sabaarya.services.data.network.RawAPI;
import com.sabaarya.services.data.network.RxErrorHandlingCallAdapterFactory;
import com.sabaarya.services.data.network.ServerAPI;
import com.sabaarya.services.data.network.interceptors.AuthenticationInterceptor;
import com.sabaarya.services.data.network.interceptors.ResponseParserInterceptor;
import com.sabaarya.services.data.preference.Preferences;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@Singleton
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient okHttpClient(Preferences preferences) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addNetworkInterceptor(new ResponseParserInterceptor())
                .addNetworkInterceptor(new AuthenticationInterceptor(preferences));
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        builder.addNetworkInterceptor(logging);
        //TODO: REMOVE FOR VALID SERVER
        builder.hostnameVerifier((hostname, session) -> true);
        return builder.build();
    }

    @Provides
    @Singleton
    @Named("raw")
    OkHttpClient rawHttpClient(Preferences preferences) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addNetworkInterceptor(new AuthenticationInterceptor(preferences));
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        builder.addNetworkInterceptor(logging);
        //TODO: REMOVE FOR VALID SERVER
        builder.hostnameVerifier((hostname, session) -> true);
        return builder.build();
    }

    @Provides
    @Singleton
    ServerAPI serverAPI(OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ServerAPI.SERVER_DOMEN)
                .build().create(ServerAPI.class);
    }

    @Provides
    @Singleton
    RawAPI rawAPI(@Named("raw") OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ServerAPI.SERVER_DOMEN)
                .build().create(RawAPI.class);
    }

}
