package com.sabaarya.services.dagger.module;

import android.app.Application;

import com.sabaarya.com.services.BuildConfig;
import com.sabaarya.services.data.database.models.Models;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

@Module
@Singleton
public class DatabaseModule {

    private static final int DATABASE_VERSION = 3;

    @Provides
    @Singleton
    ReactiveEntityStore<Persistable> persistableSingleEntityStore(Application application) {
        // override onUpgrade to handle migrating to a new version
        DatabaseSource source = new DatabaseSource(application, Models.DEFAULT, DATABASE_VERSION);
        if (BuildConfig.DEBUG) {
            // use this in development mode to drop and recreate the tables on every upgrade
            source.setTableCreationMode(TableCreationMode.DROP_CREATE);
        }
        Configuration configuration = source.getConfiguration();
        return ReactiveSupport.toReactiveStore(new EntityDataStore<Persistable>(configuration));
    }


}

