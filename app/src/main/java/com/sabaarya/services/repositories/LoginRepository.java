package com.sabaarya.services.repositories;

import com.sabaarya.services.data.network.ServerAPI;
import com.sabaarya.services.data.network.models.AuthCheck;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.network.models.User;
import com.sabaarya.services.data.network.requests.CheckEmailRequest;
import com.sabaarya.services.data.network.requests.CheckPhoneRequest;
import com.sabaarya.services.data.network.requests.ConfirmationCodeEmailRequest;
import com.sabaarya.services.data.network.requests.ConfirmationCodePhoneRequest;
import com.sabaarya.services.data.network.requests.RegisterEmailRequest;
import com.sabaarya.services.data.network.requests.RegisterPhoneRequest;
import com.sabaarya.services.data.preference.Preferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class LoginRepository {

    private final ServerAPI serverAPI;
    private final Preferences preferences;

    @Inject
    LoginRepository(ServerAPI serverAPI, Preferences preferences) {
        this.serverAPI = serverAPI;
        this.preferences = preferences;
    }

    public Flowable<Result<Boolean>> confirmEmail(String email) {
        return serverAPI.confirmEmail(new ConfirmationCodeEmailRequest(email, preferences.getLanguageAPI()));
    }

    public Flowable<Result<Boolean>> confirmPhone(String phone) {
        return serverAPI.confirmPhone(new ConfirmationCodePhoneRequest(phone, preferences.getLanguageAPI()));
    }

    public Flowable<Result<AuthCheck>> checkPhone(String confirmationCode, String phone) {
        return serverAPI.checkAccount(new CheckPhoneRequest(confirmationCode, phone, preferences.getLanguageAPI()));
    }

    public Flowable<Result<AuthCheck>> checkEmail(String confirmationCode, String email) {
        return serverAPI.checkAccount(new CheckEmailRequest(confirmationCode, email, preferences.getLanguageAPI()));
    }

    public Flowable<Result<User>> registerWithPhone(String name, String confirmationCode, String phone) {
        return serverAPI.register(new RegisterPhoneRequest(name, confirmationCode, phone, preferences.getLanguageAPI()));
    }

    public Flowable<Result<User>> registerWithEmail(String name, String confirmationCode, String email) {
        return serverAPI.register(new RegisterEmailRequest(name, confirmationCode, email, preferences.getLanguageAPI()));
    }

}
