package com.sabaarya.services.repositories;

import com.sabaarya.services.data.network.ServerAPI;
import com.sabaarya.services.data.network.models.ProfileData;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.preference.Preferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class UserRepository {

    private final ServerAPI serverAPI;
    private final Preferences preferences;

    @Inject
    UserRepository(ServerAPI serverAPI, Preferences preferences) {
        this.serverAPI = serverAPI;
        this.preferences = preferences;
    }

    public Flowable<Result<ProfileData>> getProfileInfo(int companyId, int requesterId) {
        return serverAPI.getProfileData(companyId, requesterId, preferences.getLanguageAPI());
    }

}
