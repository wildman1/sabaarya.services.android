package com.sabaarya.services.repositories;

import com.sabaarya.services.data.network.RawAPI;
import com.sabaarya.services.data.network.ServerAPI;
import com.sabaarya.services.data.network.models.AppVersion;
import com.sabaarya.services.data.network.models.Category;
import com.sabaarya.services.data.network.models.HomeData;
import com.sabaarya.services.data.network.models.Result;
import com.sabaarya.services.data.preference.Preferences;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;


@Singleton
public class HomeRepository {

    private final ServerAPI serverAPI;
    private final RawAPI rawAPI;
    private final Preferences preferences;

    @Inject
    HomeRepository(ServerAPI serverAPI, RawAPI rawAPI, Preferences preferences) {
        this.serverAPI = serverAPI;
        this.rawAPI = rawAPI;
        this.preferences = preferences;
    }

    public Flowable<Result<AppVersion>> getAppVersion() {
        return serverAPI.getVersion(preferences.getLanguageAPI());
    }

    public Flowable<List<Category>> getCategories(String url) {
        return rawAPI.getCategories(url);
    }

    public Flowable<Result<HomeData>> getHomeData(int userId) {
        return serverAPI.getHomeData(userId, preferences.getLanguageAPI());
    }

}
