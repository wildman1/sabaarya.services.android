package com.sabaarya.services.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.sabaarya.services.data.preference.Preferences;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LanguagesManager {

    private static final String TAG = "LanguagesManager";

    private static final int LANGUAGE_PERSIAN = 1;

    private Map<Integer, String> languages;
    private Preferences preferences;

    @Inject
    LanguagesManager(Preferences preferences) {
        this.preferences = preferences;
        languages = new HashMap<>();
        languages.put(0, "en");
        languages.put(LANGUAGE_PERSIAN, "fa");
    }

    private String getLanguageCode(final int id) {
        return languages.get(id);
    }

    public void setLanguage(Context context) {
        try {
            Locale locale = new Locale(getLanguageCode(preferences.getLanguage()));
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to set language", e);
        }
    }

    public boolean isFontOverriden() {
        return preferences.getLanguage() == LANGUAGE_PERSIAN;
    }

}
