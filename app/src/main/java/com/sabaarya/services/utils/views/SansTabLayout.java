package com.sabaarya.services.utils.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sabaarya.services.App;
import com.sabaarya.services.utils.LanguagesManager;

import javax.inject.Inject;


public class SansTabLayout extends TabLayout {

    private static Typeface font;
    @Inject
    LanguagesManager languagesManager;

    public SansTabLayout(Context context) {
        super(context);
    }

    public SansTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SansTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    {
        App.INSTANCE.getAppComponent().inject(this);
        if (font == null) {
            font = Typeface.createFromAsset(getContext().getAssets(), "IRANSansMobile_Large.ttf");
        }
    }

    @Override
    public void addTab(@NonNull Tab tab) {
        super.addTab(tab);
        setTextFont(tab);
    }

    @Override
    public void addTab(@NonNull Tab tab, boolean selected) {
        super.addTab(tab, selected);
        setTextFont(tab);
    }

    private void setTextFont(Tab tab) {
        if (languagesManager.isFontOverriden()) {
            ViewGroup mainView = (ViewGroup) getChildAt(0);
            ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());
            int tabChildCount = tabView.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = tabView.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }
        }
    }

}
