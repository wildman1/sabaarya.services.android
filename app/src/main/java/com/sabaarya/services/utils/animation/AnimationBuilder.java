package com.sabaarya.services.utils.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import java.util.ArrayList;


public class AnimationBuilder {

    private final RelativeLayout.LayoutParams params;
    private int duration = 300;

    private ArrayList<Animator> list = new ArrayList<>();
    private View target;

    private Animator.AnimatorListener animatorListener;

    public AnimationBuilder(View target, int duration) {
        this.target = target;
        this.duration = duration;
        params = (RelativeLayout.LayoutParams) target.getLayoutParams();

    }

    public AnimationBuilder setAnimatorListener(Animator.AnimatorListener animatorListener) {
        this.animatorListener = animatorListener;
        return this;
    }

    public AnimationBuilder setTopMarginAnim(int from, int to) {
        ValueAnimator animatorTop = ValueAnimator.ofInt(from, to);
        animatorTop.addUpdateListener(valueAnimator -> {
            params.topMargin = (Integer) valueAnimator.getAnimatedValue();
            target.requestLayout();
        });
        animatorTop.setDuration(duration);
        list.add(animatorTop);
        return this;
    }

    public AnimationBuilder setLeftMarginAnim(int from, int to) {
        ValueAnimator animatorTop = ValueAnimator.ofInt(from, to);
        animatorTop.addUpdateListener(valueAnimator -> {
            params.leftMargin = (Integer) valueAnimator.getAnimatedValue();
            target.requestLayout();
        });
        animatorTop.setDuration(duration);
        list.add(animatorTop);
        return this;
    }

    public AnimationBuilder setRightMarginAnim(int from, int to) {
        ValueAnimator animatorTop = ValueAnimator.ofInt(from, to);
        animatorTop.addUpdateListener(valueAnimator -> {
            params.rightMargin = (Integer) valueAnimator.getAnimatedValue();
            target.requestLayout();
        });
        animatorTop.setDuration(duration);
        list.add(animatorTop);
        return this;
    }

    public AnimationBuilder setTranslationYAnim(float from, float to) {
        ObjectAnimator animTranslationY = ObjectAnimator.ofFloat(target, "translationY", from, to);
        animTranslationY.setDuration(duration);
        TimeInterpolator timeInterpolator = new LinearInterpolator();
        animTranslationY.setInterpolator(timeInterpolator);
        list.add(animTranslationY);
        return this;

    }

    public AnimationBuilder setAlphaAnim(float from, float to) {
        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(target, "alpha", from, to);
        animAlpha.setDuration(duration);
        list.add(animAlpha);
        return this;

    }

    public AnimatorSet toTogetherAnimatorSet() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(list);
        if (animatorListener != null) {
            animatorSet.addListener(animatorListener);
        }
        return animatorSet;
    }

    public AnimatorSet toSequentiallyAnimatorSet() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(list);
        if (animatorListener != null) {
            animatorSet.addListener(animatorListener);
        }
        return animatorSet;
    }

    public AnimationBuilder setBottomMarginAnim(int from, int to) {
        ValueAnimator animatorTop = ValueAnimator.ofInt(from, to);
        animatorTop.addUpdateListener(valueAnimator -> {
            params.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
            target.requestLayout();
        });
        animatorTop.setDuration(duration);
        list.add(animatorTop);
        return this;
    }

    public AnimationBuilder setHeightAnim(int from, int to) {
        ValueAnimator animatorTop = ValueAnimator.ofInt(from, to);
        animatorTop.addUpdateListener(valueAnimator -> {
            params.height = (Integer) valueAnimator.getAnimatedValue();
            target.requestLayout();
        });
        animatorTop.setDuration(duration);
        list.add(animatorTop);
        return this;
    }
}
