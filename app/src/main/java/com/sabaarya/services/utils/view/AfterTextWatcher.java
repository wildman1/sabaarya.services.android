package com.sabaarya.services.utils.view;

import android.support.annotation.NonNull;
import android.text.Editable;

import io.reactivex.functions.Consumer;

/**
 * TextWatcher with Action
 */
public class AfterTextWatcher extends DefaultTextWatcher {

    private final Consumer<String> consumer;

    public AfterTextWatcher(@NonNull Consumer<String> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            consumer.accept(s.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
