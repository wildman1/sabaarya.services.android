package com.sabaarya.services.utils;


import android.widget.ImageView;

import com.sabaarya.com.services.R;
import com.squareup.picasso.Picasso;

public class ImageUtils {

    public static void loadImageCenterCrop(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    public static void loadImageProfile(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .error(R.drawable.ic_profile)
                .placeholder(R.drawable.ic_profile)
                .fit()
                .centerCrop()
                .into(imageView);
    }


    public static void loadImageCenterInside(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .fit()
                .centerInside()
                .into(imageView);
    }

}
