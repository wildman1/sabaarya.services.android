package com.sabaarya.services.utils;


import android.view.View;

public class ViewUtils {

    public static void hideViews(View... views) {
        updateViewsVisibility(View.GONE, views);
    }

    public static void showViews(View... views) {
        updateViewsVisibility(View.VISIBLE, views);
    }

    private static void updateViewsVisibility(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

}
