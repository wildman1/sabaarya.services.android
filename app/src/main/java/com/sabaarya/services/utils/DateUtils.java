package com.sabaarya.services.utils;


import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final SimpleDateFormat OUTPUT_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private static final SimpleDateFormat INFO_FORMAT = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    private static final SimpleDateFormat DATE_FORMAT_API = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss", Locale.getDefault());


    public static String formatOutputDate(Date date) {
        return OUTPUT_FORMAT.format(date);
    }

    public static Date parseOutputDate(String date) {
        try {
            return OUTPUT_FORMAT.parse(date);
        } catch (ParseException e) {
            Log.e("Date Utils", "Failed to parse output date", e);
            return null;
        }
    }

    public static String formatInfoDate(String apiDate) {
        try {
            return INFO_FORMAT.format(DATE_FORMAT_API.parse(apiDate));
        } catch (ParseException |NullPointerException e) {
            return apiDate;
        }
    }

}
